<?php

namespace CreativeFolio\WelcomeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/*
Le fichier DefaultController.php est ce que l'on appelle un contrôleur.
Ce fichier récupère les informations HTTP comme le détail de l'URL, les
données de formulaire ou de session et affiche un message ou une page web.
Ici, ce fichier contient une seule action qui est d'afficher le message
"Mon premier message". Notre application a pour le moment un seul contrôleur
et une seule action. À terme, elle aura plusieurs contrôleurs qui contiendront
chacun plusieurs actions. Par exemple, nous aurons le contrôleur "ActeurController"
qui gérera l'ajout, la modification et la liste des acteurs qui jouent
dans les films. Par contre, nous n'utiliserons plus la fonction "echo"
pour l'affichage mais des fichiers templates, c'est ce que nous verrons
un peu plus loin.
*/

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('WelcomeBundle:Default:index.html.twig');
    }
}