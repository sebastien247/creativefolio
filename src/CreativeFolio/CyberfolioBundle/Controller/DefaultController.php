<?php

namespace CreativeFolio\CyberfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CreativeFolioCyberfolioBundle:Default:index.html.twig', array('name' => $name));
    }
}
