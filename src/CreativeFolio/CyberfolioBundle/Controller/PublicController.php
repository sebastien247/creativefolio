<?php

namespace CreativeFolio\CyberfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class PublicController extends Controller
{
    public function indexAction()
    {
        //return $this->render('CreativeFolioCyberfolioBundle:Public:index.html.twig');
        return new Response("Page index cyberfolio");
    }
}
