<?php

namespace CreativeFolio\UtilisateurBundle\Entity;
use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Utilisateur
 */
class Utilisateur extends BaseUser 
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \CreativeFolio\UtilisateurBundle\Entity\Ville
     */
    protected $ville;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ville
     *
     * @param \CreativeFolio\UtilisateurBundle\Entity\Ville $ville
     * @return Utilisateur
     */
    public function setVille(\CreativeFolio\UtilisateurBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;
    
        return $this;
    }

    /**
     * Get ville
     *
     * @return \CreativeFolio\UtilisateurBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }
}
