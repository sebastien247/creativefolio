<?php

namespace CreativeFolio\UtilisateurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Departement
 */
class Departement
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var \CreativeFolio\UtilisateurBundle\Entity\Region
     */
    private $region;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param integer $code
     * @return Departement
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return integer 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Departement
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set region
     *
     * @param \CreativeFolio\UtilisateurBundle\Entity\Region $region
     * @return Departement
     */
    public function setRegion(\CreativeFolio\UtilisateurBundle\Entity\Region $region = null)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return \CreativeFolio\UtilisateurBundle\Entity\Region 
     */
    public function getRegion()
    {
        return $this->region;
    }
}