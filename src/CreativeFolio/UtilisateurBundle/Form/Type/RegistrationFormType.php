<?php

namespace CreativeFolio\UtilisateurBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
		public function buildForm(FormBuilderInterface $builder, array $options)
		{
				parent::buildForm($builder, $options);

				$builder->add('ville', 'genemu_jqueryautocomplete_entity', array(
						'class' => 'CreativeFolio\UtilisateurBundle\Entity\Ville',
						'route_name' => 'utilisateur_ajax_ville', //Nom de la route pour executer la requete de recherche dans la bdd
						'label' => 'Ville : ',
						'configs' => array(
								'minLength' => 0,
						),
						'required' => false
				));


		        /*$builder->add('ville', 'genemu_jqueryselect2_hidden', array(
		            'configs' => array(
		                'multiple' => false // Wether or not multiple values are allowed (default to false)
		            )	
		        ));

				$builder->add('ville', 'genemu_jqueryselect2_entity', array(
		            'class' => 'CreativeFolio\UtilisateurBundle\Entity\Ville',
		            'property' => 'nom',
		            'configs' => array(
		                'minimumInputLength' => 2, // Wether or not multiple values are allowed (default to false)
		                'placeholder' => 'Votre ville',
		                'allowClear' => true,
		                'width' => '250px'
		            )
		        ));*/


				/*$builder->add('ville', 'entity_ajax', array(
						'class' => 'CreativeFolio\UtilisateurBundle\Entity\Ville',
						'label' => 'Ville : ',
						'property' => 'nom',
						'required' => false
				));*/



				//var_dump($builder);

		}

		public function getName()
		{
				return 'CreativeFolio_user_registration';
		}
}