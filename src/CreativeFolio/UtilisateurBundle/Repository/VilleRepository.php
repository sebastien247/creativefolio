<?php
namespace CreativeFolio\UtilisateurBundle\Repository;
 
use Doctrine\ORM\EntityRepository;
 
class VilleRepository extends EntityRepository
{
    /**
     * Find By Name With Like Attribute
     *
     * @return Array
     */
    public function _findByAjaxName ($term, $limit=10)
    {
        $qb = $this->createQueryBuilder('c');
 
        $qb ->select('c')
            ->where('c.nom LIKE :term')
            ->setMaxResults($limit)
            ->setParameter('term', $term.'%');
 
        $arrayAss = $qb->getQuery()->getArrayResult();
 
        // Transformer le tableau associatif en un tableau standard
        $models = Array();
        foreach($arrayAss as $data)
        {
            //$o = new $this->_entityName(); // setId impossible
            foreach ($data as $key => $value)
            {
                $model['id'] = $data['id'];
                $model['nom'] = $data['nom'];
                $model['codePostal'] = $data['codePostal'];
                $model['latitude'] = $data['latitude'];
                $model['longitude'] = $data['longitude'];
            }

            $models[] = $model;
        }
        return $models;
    }
}
?>