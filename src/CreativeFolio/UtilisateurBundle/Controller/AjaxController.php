<?php

namespace CreativeFolio\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CreativeFolio\UtilisateurBundle\Entity as Entity;

class AjaxController extends Controller 
{
    public function villeAction(Request $request)
    {
        $value = $request->get('term');

        $villes = $this->getDoctrine()
			->getEntityManager()
			->getRepository('UtilisateurBundle:Ville')
			->_findByAjaxName($value);

        $json = array();
        foreach ($villes as $ville) {
            $json[] = array(
                'value' => $ville['nom'],
                'id' => $ville['id']
            );
        }

        $response = new Response();
        $response->setContent(json_encode($json));

        return $response;
    }
}
