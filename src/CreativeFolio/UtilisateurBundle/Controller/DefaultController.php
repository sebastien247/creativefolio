<?php

namespace CreativeFolio\UtilisateurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

//use Symfony\Component\DependencyInjection\ContainerAware;
//use Symfony\Component\HttpFoundation\RedirectResponse;
//use Symfony\Component\HttpFoundation\Response;
//use CreativeFolio\UtilisateurBundle\Entity\Ville;

class DefaultController extends Controller 
{
    public function indexAction($name)
    {
        return $this->render('UtilisateurBundle:Default:index.html.twig', array('name' => $name));
    }
}
