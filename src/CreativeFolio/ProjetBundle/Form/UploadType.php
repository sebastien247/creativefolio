<?php

namespace CreativeFolio\ProjetBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityRepository;

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('description')
            //->add('dateajout')
            ->add('file', 'file') // Ajout du fichier, champ n'apparaissant pas dans l'entité et le bdd
            //on de demande pas le path a l'utilisateur
            //->add('path')
            //->add('projet')
            ->add('projet', 
              'entity', 
              array('class' => 'CreativeFolio\ProjetBundle\Entity\Projet',
                    'query_builder' => function(EntityRepository $er) use($options) {
                        //return $er->findByUser($options['user']);
                        return $er->createQueryBuilder('projet')
                                  ->where('projet.user = :userId')
                                  ->setParameter('userId', $options['userId']);
                    },
                    'required' => true, 
                    'empty_value' => false,
              ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CreativeFolio\ProjetBundle\Entity\Upload',
            'userId'     => null,
            'user'       => null
        ));
    }

    public function getName()
    {
        return 'creativefolio_projetbundle_uploadtype';
    }
}
