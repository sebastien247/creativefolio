<?php

namespace CreativeFolio\ProjetBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Doctrine\Common\Collections\ArrayCollection;

use CreativeFolio\ProjetBundle\Entity\Upload;
use CreativeFolio\ProjetBundle\Form\UploadType;

/**
 * Upload controller.
 *
 */
class UploadController extends Controller
{
    /**
     * Lists all Upload entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ProjetBundle:Upload')->findAll();

        return $this->render('ProjetBundle:Upload:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Upload entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProjetBundle:Upload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Upload entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ProjetBundle:Upload:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Upload entity.
     *
     */
    public function newAction()
    {
        $entity = new Upload();

        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $projets = $em->getRepository('ProjetBundle:Projet')->findByUser($user);

        $options = array( 'userId' => $user->getId(), 'user' => $user );

        $form   = $this->createForm(new UploadType(), $entity, $options );
        
        return $this->render('ProjetBundle:Upload:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Upload entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Upload();

        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $options = array( 'userId' => $user->getId(), 'user' => $user );

        $form   = $this->createForm(new UploadType(), $entity, $options );

        if ($this->getRequest()->isMethod('POST')) {
            $form->bind($this->getRequest());
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                // la propriété « file » peut être vide si le champ n'est pas requis
                if (null === $entity->file) {
                    return;
                }

                // utilisez le nom de fichier original ici mais
                // vous devriez « l'assainir » pour au moins éviter
                // quelconques problèmes de sécurité
                $prefixe = time();
                $name = $prefixe ;
                // la méthode « move » prend comme arguments le répertoire cible et
                // le nom de fichier cible où le fichier doit être déplacé
                $name .= $entity->file->getClientOriginalName() ;
                $user = $this->container->get('security.context')->getToken()->getUser();
                // path personnalise avec l'id puis le nom du proj puis le fichier upload
                $newPath = $entity->getUploadDir().'/'.$user->getId().'/'.$entity->getProjet() ;
                $entity->file->move($newPath, $name);

                // définit la propriété « path » comme étant le nom de fichier où vous
                // avez stocké le fichier
                $entity->setPath($newPath.$entity->file->getClientOriginalName());

                // « nettoie » la propriété « file » comme vous n'en aurez plus besoin
                $entity->file = null;

                $em->persist($entity);
                $em->flush();

                return $this->redirect($this->generateUrl('upload_show', array('id' => $entity->getId())));
            }
        }

        return $this->render('ProjetBundle:Upload:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Upload entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProjetBundle:Upload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Upload entity.');
        }

        $editForm = $this->createForm(new UploadType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('ProjetBundle:Upload:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Upload entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ProjetBundle:Upload')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Upload entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new UploadType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('upload_edit', array('id' => $id)));
        }

        return $this->render('ProjetBundle:Upload:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Upload entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ProjetBundle:Upload')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Upload entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('upload'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
