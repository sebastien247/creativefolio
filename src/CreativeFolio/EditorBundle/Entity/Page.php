<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 */
class Page
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Cyberfolio
     */
    private $cyberfolio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bloc;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bloc = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cyberfolio
     *
     * @param \CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio
     * @return Page
     */
    public function setCyberfolio(\CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio = null)
    {
        $this->cyberfolio = $cyberfolio;
    
        return $this;
    }

    /**
     * Get cyberfolio
     *
     * @return \CreativeFolio\EditorBundle\Entity\Cyberfolio 
     */
    public function getCyberfolio()
    {
        return $this->cyberfolio;
    }

    /**
     * Add bloc
     *
     * @param \CreativeFolio\EditorBundle\Entity\Bloc $bloc
     * @return Page
     */
    public function addBloc(\CreativeFolio\EditorBundle\Entity\Bloc $bloc)
    {
        $this->bloc[] = $bloc;
    
        return $this;
    }

    /**
     * Remove bloc
     *
     * @param \CreativeFolio\EditorBundle\Entity\Bloc $bloc
     */
    public function removeBloc(\CreativeFolio\EditorBundle\Entity\Bloc $bloc)
    {
        $this->bloc->removeElement($bloc);
    }

    /**
     * Get bloc
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBloc()
    {
        return $this->bloc;
    }
    /**
     * @var string
     */
    private $nom;


    /**
     * Set nom
     *
     * @param string $nom
     * @return Page
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }
}