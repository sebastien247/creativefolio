<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cyberfolio
 */
class Cyberfolio
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var string
     */
    private $url;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Layout
     */
    private $layout;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Menu
     */
    private $menu;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Style
     */
    private $style;

    /**
     * @var \CreativeFolio\UtilisateurBundle\Entity\Utilisateur
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $categorie;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categorie = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Cyberfolio
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Cyberfolio
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set layout
     *
     * @param \CreativeFolio\EditorBundle\Entity\Layout $layout
     * @return Cyberfolio
     */
    public function setLayout(\CreativeFolio\EditorBundle\Entity\Layout $layout = null)
    {
        $this->layout = $layout;
    
        return $this;
    }

    /**
     * Get layout
     *
     * @return \CreativeFolio\EditorBundle\Entity\Layout 
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set menu
     *
     * @param \CreativeFolio\EditorBundle\Entity\Menu $menu
     * @return Cyberfolio
     */
    public function setMenu(\CreativeFolio\EditorBundle\Entity\Menu $menu = null)
    {
        $this->menu = $menu;
    
        return $this;
    }

    /**
     * Get menu
     *
     * @return \CreativeFolio\EditorBundle\Entity\Menu 
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set style
     *
     * @param \CreativeFolio\EditorBundle\Entity\Style $style
     * @return Cyberfolio
     */
    public function setStyle(\CreativeFolio\EditorBundle\Entity\Style $style = null)
    {
        $this->style = $style;
    
        return $this;
    }

    /**
     * Get style
     *
     * @return \CreativeFolio\EditorBundle\Entity\Style 
     */
    public function getStyle()
    {
        return $this->style;
    }

    /**
     * Set user
     *
     * @param \CreativeFolio\UtilisateurBundle\Entity\Utilisateur $user
     * @return Cyberfolio
     */
    public function setUser(\CreativeFolio\UtilisateurBundle\Entity\Utilisateur $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \CreativeFolio\UtilisateurBundle\Entity\Utilisateur 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add categorie
     *
     * @param \CreativeFolio\EditorBundle\Entity\Categorie $categorie
     * @return Cyberfolio
     */
    public function addCategorie(\CreativeFolio\EditorBundle\Entity\Categorie $categorie)
    {
        $this->categorie[] = $categorie;
    
        return $this;
    }

    /**
     * Remove categorie
     *
     * @param \CreativeFolio\EditorBundle\Entity\Categorie $categorie
     */
    public function removeCategorie(\CreativeFolio\EditorBundle\Entity\Categorie $categorie)
    {
        $this->categorie->removeElement($categorie);
    }

    /**
     * Get categorie
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    public function __toString(){
        return $this->nom;
    }
}