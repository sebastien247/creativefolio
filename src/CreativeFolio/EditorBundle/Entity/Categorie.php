<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categorie
 */
class Categorie
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $cyberfolio;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cyberfolio = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Categorie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Add cyberfolio
     *
     * @param \CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio
     * @return Categorie
     */
    public function addCyberfolio(\CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio)
    {
        $this->cyberfolio[] = $cyberfolio;
    
        return $this;
    }

    /**
     * Remove cyberfolio
     *
     * @param \CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio
     */
    public function removeCyberfolio(\CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio)
    {
        $this->cyberfolio->removeElement($cyberfolio);
    }

    /**
     * Get cyberfolio
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCyberfolio()
    {
        return $this->cyberfolio;
    }

    public function __toString(){
        return $this->nom;
    }
}