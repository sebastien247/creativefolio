<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Style
 */
class Style
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $css;

    /**
     * @var string
     */
    private $cssSaved;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->css = json_encode(array("global" => "", "blocs" => ""));
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set css
     *
     * @param string $css
     * @return Style
     */
    public function setCss($css)
    {
        $this->css = $css;
    
        return $this;
    }

    /**
     * Get css
     *
     * @return string 
     */
    public function getCss()
    {
        return $this->css;
    }

    /**
     * Set cssSaved
     *
     * @param string $cssSaved
     * @return Style
     */
    public function setCssSaved($cssSaved)
    {
        $this->cssSaved = $cssSaved;
    
        return $this;
    }

    /**
     * Get cssSaved
     *
     * @return string 
     */
    public function getCssSaved()
    {
        return $this->cssSaved;
    }
}