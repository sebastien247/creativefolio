<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bloc
 */
class Bloc
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $poid;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Bloc
     */
    private $bloc;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Cyberfolio
     */
    private $cyberfolio;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\Layout
     */
    private $layout;

    /**
     * @var \CreativeFolio\EditorBundle\Entity\BlocType
     */
    private $blocType;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $page;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->page = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Bloc
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set poid
     *
     * @param integer $poid
     * @return Bloc
     */
    public function setPoid($poid)
    {
        $this->poid = $poid;
    
        return $this;
    }

    /**
     * Get poid
     *
     * @return integer 
     */
    public function getPoid()
    {
        return $this->poid;
    }

    /**
     * Set bloc
     *
     * @param \CreativeFolio\EditorBundle\Entity\Bloc $bloc
     * @return Bloc
     */
    public function setBloc(\CreativeFolio\EditorBundle\Entity\Bloc $bloc = null)
    {
        $this->bloc = $bloc;
    
        return $this;
    }

    /**
     * Get bloc
     *
     * @return \CreativeFolio\EditorBundle\Entity\Bloc 
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * Set cyberfolio
     *
     * @param \CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio
     * @return Bloc
     */
    public function setCyberfolio(\CreativeFolio\EditorBundle\Entity\Cyberfolio $cyberfolio = null)
    {
        $this->cyberfolio = $cyberfolio;
    
        return $this;
    }

    /**
     * Get cyberfolio
     *
     * @return \CreativeFolio\EditorBundle\Entity\Cyberfolio 
     */
    public function getCyberfolio()
    {
        return $this->cyberfolio;
    }

    /**
     * Set layout
     *
     * @param \CreativeFolio\EditorBundle\Entity\Layout $layout
     * @return Bloc
     */
    public function setLayout(\CreativeFolio\EditorBundle\Entity\Layout $layout = null)
    {
        $this->layout = $layout;
    
        return $this;
    }

    /**
     * Get layout
     *
     * @return \CreativeFolio\EditorBundle\Entity\Layout 
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * Set blocType
     *
     * @param \CreativeFolio\EditorBundle\Entity\BlocType $blocType
     * @return Bloc
     */
    public function setBlocType(\CreativeFolio\EditorBundle\Entity\BlocType $blocType = null)
    {
        $this->blocType = $blocType;
    
        return $this;
    }

    /**
     * Get blocType
     *
     * @return \CreativeFolio\EditorBundle\Entity\BlocType 
     */
    public function getBlocType()
    {
        return $this->blocType;
    }

    /**
     * Add page
     *
     * @param \CreativeFolio\EditorBundle\Entity\Page $page
     * @return Bloc
     */
    public function addPage(\CreativeFolio\EditorBundle\Entity\Page $page)
    {
        $this->page[] = $page;
    
        return $this;
    }

    /**
     * Remove page
     *
     * @param \CreativeFolio\EditorBundle\Entity\Page $page
     */
    public function removePage(\CreativeFolio\EditorBundle\Entity\Page $page)
    {
        $this->page->removeElement($page);
    }

    /**
     * Get page
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPage()
    {
        return $this->page;
    }

    public function __toString(){
        return $this->nom;
    }
}