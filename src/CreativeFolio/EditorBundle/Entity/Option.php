<?php

namespace CreativeFolio\EditorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Option
 */
class Option
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $grid;

    /**
     * @var \CreativeFolio\UtilisateurBundle\Entity\Utilisateur
     */
    private $user;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return Option
     */
    public function setUrl($url)
    {
        $this->url = $url;
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set grid
     *
     * @param string $grid
     * @return Option
     */
    public function setGrid($grid)
    {
        $this->grid = $grid;
    
        return $this;
    }

    /**
     * Get grid
     *
     * @return string 
     */
    public function getGrid()
    {
        return $this->grid;
    }

    /**
     * Set user
     *
     * @param \CreativeFolio\UtilisateurBundle\Entity\Utilisateur $user
     * @return Option
     */
    public function setUser(\CreativeFolio\UtilisateurBundle\Entity\Utilisateur $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \CreativeFolio\UtilisateurBundle\Entity\Utilisateur 
     */
    public function getUser()
    {
        return $this->user;
    }
}