(function (i, l) {
    function C(a) {
        if (!a) a = i.event;
        if (a.shiftKey && a.keyCode > 34 && a.keyCode < 41) a.returnValue = true;
        else if (a.keyCode === 38 || a.keyCode === 40) {
            a.returnValue = g;
            var b = a.target,
                f = b.value * 1;
            if (a.keyCode === 38) f++;
            else a.keyCode === 40 && f > 0 && f--;
            l[b.id] = c[b.id] = b.value = f;
            p();
            m();
            q()
        } else a.returnValue = a.keyCode === 32 || a.keyCode > 64 && a.keyCode < 91 || a.keyCode > 105 && a.keyCode < 112 || a.keyCode > 185 && a.keyCode < 223 || a.keyCode > 47 && a.keyCode < 60 && (a.shiftKey || a.altKey) ? g : true
    }
    function D(a) {
        if (!a) a = i.event;
        a = a.target;
        l[a.id] = c[a.id] = a.value * 1;
        p();
        m();
        q()
    }
    function E(a) {
        if (!a) a = i.event;
        a = a.target;
        if (a.value === "") a.value = "0"
    }
    function F(a) {
        if (!a) a = i.event;
        j = a.target;
        d.addEventListener("mousewheel", r, g);
        d.addEventListener("DOMMouseScroll", r, g)
    }
    function G() {
        d.removeEventListener("mousewheel", r, g);
        d.removeEventListener("DOMMouseScroll", r, g)
    }
    function r(a) {
        var b = 0;
        if (!a) a = i.event;
        if (a.wheelDelta) b = a.wheelDelta;
        else if (a.detail) b = -a.detail;
        var f = j.value * 1;
        if (b > 0) l[j.id] = c[j.id] = j.value = f + 1;
        else if (b < 0) if (f > 0) l[j.id] = c[j.id] =
            j.value = f - 1;
        p();
        m();
        q();
        a.returnValue = g
    }
    function q() {
        v.href = "/patterns/" + c.baseline + "-" + c.gutter + "-" + c.module_width + "-" + c.module_height + "-photoshop.pat"
    }
    function m() {
        var a = (c.module_width + c.gutter) * c.modules - c.gutter;
        if (a < 0) a = 0;
        w.value = a;
        var b = (d.body.offsetWidth - a) / 2;
        if (b < 0) b = a = 0;
        else if (b & false) b = a = b;
        else {
            a = Math.ceil(b);
            b = Math.floor(b)
        }
        a = a + t;
        b = b + t;
        u.style.backgroundPosition = a + " 207" + t;
        s.style.backgroundPosition = a + " 0";
        s.firstChild.style.marginLeft = a;
        x.style.width = a;
        y.style.width = b
    }
    function p() {
        var a =
            c.module_width + c.gutter,
            b = c.module_height + 1,
            f = c.baseline * b,
            n = c.baseline * c.module_height;
        k.width = a;
        k.height = f;
        if (k.getContext) {
            var e = k.getContext("2d");
            e.fillStyle = "rgba(242,5,13,0.12)";
            e.fillRect(0, 0, a, f);
            e.fillStyle = "rgba(242,5,13,0.3)";
            e.clearRect(0, 0, c.module_width, n);
            e.fillRect(0, 0, c.module_width, n);
            e.clearRect(c.module_width, n, c.gutter, c.baseline);
            e.fillStyle = "rgba(217,4,11,0.77)";
            for (var h = 1; h <= b; h++) {
                e.clearRect(0, c.baseline * h - 1, a, 1);
                e.fillRect(0, c.baseline * h - 1, a, 1)
            }
            h = k.toDataURL("image/png");
            u.style.backgroundImage = "url(" + h + ")";
            z.href = h;
            e.clearRect(0, 0, a, f);
            e.fillStyle = "#202020";
            e.fillRect(0, 0, a, f);
            e.fillStyle = "#4d4d4d";
            e.fillRect(0, 0, c.module_width, n);
            e.fillStyle = "#000000";
            e.fillRect(c.module_width, n, c.gutter, c.baseline);
            e.fillStyle = "#c4c4c4";
            for (h = 1; h <= b; h++) e.fillRect(0, c.baseline * h - 1, a, 1);
            h = k.toDataURL("image/png");
            A.href = h
        } else return g
    }
    var c = {
        baseline: 18,
        gutter: 18,
        module_width: 66,
        module_height: 4,
        modules: 10
    }, t = "px",
        o = i.location,
        d, g = false,
        u, x, y, s, k, z, v, A, w, j, B = /(\?|\&){1}fb_xd_fragment(\=)?/;
    i.addEventListener("load", function () {
        d = i.document;
        u = d.getElementById("grid");
        x = d.getElementById("left_blind");
        y = d.getElementById("right_blind");
        s = d.getElementById("rule");
        k = d.getElementById("pattern");
        z = d.getElementById("png");
        v = d.getElementById("pat");
        A = d.getElementById("mask");
        w = d.getElementById("layout_width");
        for (var a in c) {
            var b = d.getElementById(a);
            if (b) {
                if (l[a]) c[a] = l[a] * 1;
                b.value = c[a];
                b.addEventListener("keydown", C, g);
                b.addEventListener("keyup", D, g);
                b.addEventListener("change", E, g);
                b.addEventListener("focus",
                F, g);
                b.addEventListener("blur", G, g)
            }
        }
        a = screen.width;
        for (b = 0; b < a; b += 50) {
            var f = d.createElement("li");
            f.appendChild(d.createTextNode(b));
            s.appendChild(f)
        }
        p();
        m();
        q()
    }, g);
    i.addEventListener("resize", m, g);
    if (o.href.match(B) != null) o.href = o.href.replace(B, "");
    if (o.hash == "") o.hash = "#app"
})(window, localStorage);