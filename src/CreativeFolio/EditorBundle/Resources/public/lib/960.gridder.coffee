#
# * Release: 1.3.1 2009-04-26
# 

#
# * Copyright (c) Andr?e Hansson (peolanha AT gmail DOT com)
# * MIT License - http://www.opensource.org/licenses/mit-license.php
# * Idea loosely based on JASH, http://billyreisinger.com/jash/
# *
# * Website: http://gridder.andreehansson.se/
# *
# * Changelog:
# * - New GUI! The new GUI should be less obtrusive and has been repositioned.
# *	 It is also featuring a slight delay on inputs so that you'll have a chance
# *	 to change the settings before it is re-rendering the grid
# * - Due to a lot of inquries regarding affiliation with $ the filenames has
# *	 been changed, I'm very sorry for the inconvenience!
# * - CSS issues with the GUI should also be fixed in more websites, please report
# *	 in any issue you stumble upon
# * - A small bug in IE that made the paragraph lines not position correctly has been
# *	 fixed
# * - A dropdown box has replaced the columns input box, 960 Gridder calculates the
# *	 proper number of columns that can be used with the specified grid width
# * - The 960 Gridder is now displaying perfectly (into the very pixels) in all
# *	 A-grade browsers (according to browsershots.org)
# * - An option to invert the gutters has been added, set this to 'true' if
# *	 you want to use it, OR use the shortcut CTRL+ALT+A
# * - Some other minor changes...
# 
(($) ->
	$.fn.Grid = (settings) ->
		c = this
		c.settingsDef =
			gColor: "#EEEEEE"
			gColumns: 12
			gOpacity: 0.45
			gWidth: 10
			pColor: "#C0C0C0"
			pHeight: 15
			pOffset: 0
			pOpacity: 0.55
			center: true
			invert: false
			gEnabled: true
			pEnabled: true
			size: 960
			fixFlash: true
			setupEnabled: true
			pressedKeys: []
			delayTimer: ""
			showGridTo: "body"
			showSetupTo: "body"

		c.oSettings = $.extend(c.settingsDef, settings)

		c._createEntity = (e, d) ->
			$("<div class=\"g-" + e + "\">&nbsp;</div>").appendTo("#g-grid").css d

		c._setVariable = (d, e) ->
			d = d.replace(/g-setup-/, "")
			if isNaN(parseInt(e, 10)) or parseInt(e, 10) is 0
				c.oSettings[d] = e
			else
				c.oSettings[d] = parseInt(e, 10)
			if e is true
				$("#g-setup-" + d).prop("checked", true);
			else
				if e is false
					$("#g-setup-" + d).prop("checked", false);
				else
					$("#g-setup-" + d).val e

		c.setupWindow = ->
			c.oSettings.height = $(document).height()

			if c.oSettings.setupEnabled
				d = 2

				while d < 48
					$("<option value=\"" + d + "\">" + d + "</option>").appendTo "#g-setup-gColumns"	if Math.round((c.oSettings.size / d)) is (c.oSettings.size / d)
					d++

				for d of c.oSettings
					if $("#g-setup-" + d).length isnt 0
						if $("#g-setup-" + d).parent().parent().is("#g-setup-misc") and c.oSettings[d]
							$("#g-setup-" + d).prop("checked", true);
						else
							$("#g-setup-" + d).val c.oSettings[d]

				$("#g-setup input").change ->
					c.setVariable $(this).attr("id"), $(this).val()
				$("#g-setup input").keyup ->
					c.setVariable $(this).attr("id"), $(this).val()

				$("#g-setup-gColumns").change ->
					c.setVariable "gColumns", $(this).val()

				$("#g-setup-misc input").click ->
					c.setVariable $(this).attr("id"), $(this).is(':checked')

				$().keydown (f) ->
					c.oSettings.pressedKeys.push f.which	if $.inArray(f.which, c.oSettings.pressedKeys) is -1

				$(window).scroll ->
					$("#g-setup").css "top", $().scrollTop() + 150

			$().keyup (g) ->
				if $.inArray(17, c.oSettings.pressedKeys) isnt -1 and $.inArray(18, c.oSettings.pressedKeys) isnt -1
					if $.inArray(90, c.oSettings.pressedKeys) isnt -1
						c.setVariable "gEnabled", not c.oSettings.gEnabled
					else
						if $.inArray(88, c.oSettings.pressedKeys) isnt -1
							c.setVariable "pEnabled", not c.oSettings.pEnabled
						else
							if $.inArray(65, c.oSettings.pressedKeys) isnt -1
								c.setVariable "invert", not c.oSettings.invert
							else
								if $.inArray(67, c.oSettings.pressedKeys) isnt -1
									c.setVariable
										gEnabled: not c.oSettings.gEnabled
										pEnabled: not c.oSettings.pEnabled

				f = $.inArray(g.which, c.oSettings.pressedKeys)
				c.oSettings.pressedKeys.splice f, f


		c.setVariable = (arg0, arg1) ->
			if typeof (arg0) is "object"
				for d of arg0
					c._setVariable d, arg0[d]
			else
				c._setVariable arg0, arg1
			c.createGrid()

		c.createGrid = ->
			$("embed").each ->
				if c.oSettings.fixFlash
					$(this).attr "wmode", "transparent"
				else
					$(this).removeAttr "wmode"
				i = $(this).wrap("<div></div>").parent().html()
				$(this).parent().replaceWith i
				$(this).remove()

			$("#g-grid").remove()
			$("<div id=\"g-grid\"></div>").appendTo(c.oSettings.showGridTo).css "width", c.oSettings.size
			if c.oSettings.center
				$("#g-grid").css
					left: "50%"
					marginLeft: -((c.oSettings.size / 2) + c.oSettings.gWidth)

			if c.oSettings.gEnabled and c.oSettings.gColumns > 0
				if c.oSettings.invert
					$().css "overflow-x", "hidden"
					e = ($(window).width() - c.oSettings.size) / 2
					c._createEntity "vertical",
						left: -e
						width: e
						height: c.oSettings.height
						backgroundColor: c.oSettings.gColor
						opacity: c.oSettings.gOpacity

					g = 0

					while g < c.oSettings.gColumns
						f = (c.oSettings.size / c.oSettings.gColumns) - (c.oSettings.gWidth)
						h = (c.oSettings.gWidth)
						c._createEntity "vertical",
							left: ((f + h) * g) + h
							width: f + "px"
							height: c.oSettings.height
							backgroundColor: c.oSettings.gColor
							opacity: c.oSettings.gOpacity

						g++
					e -= 10	if (c.oSettings.height + 10) > $(window).height()
					c._createEntity "vertical",
						left: "100%"
						marginLeft: 20
						width: e
						height: c.oSettings.height
						backgroundColor: c.oSettings.gColor
						opacity: c.oSettings.gOpacity

				else
					g = 0

					while g <= c.oSettings.gColumns
						c._createEntity "vertical",
							left: ((c.oSettings.size / c.oSettings.gColumns) * g)
							width: (c.oSettings.gWidth)
							height: c.oSettings.height
							backgroundColor: c.oSettings.gColor
							opacity: c.oSettings.gOpacity

						g++
			if c.oSettings.pEnabled and c.oSettings.pHeight > 1
				d = ((c.oSettings.height - c.oSettings.pOffset) / c.oSettings.pHeight)
				g = 0
				while g <= d
					c._createEntity "horizontal",
						top: ((c.oSettings.height / d) * g) + c.oSettings.pOffset
						left: "50%"
						marginLeft: -(c.oSettings.size / 2)
						width: (c.oSettings.size + (c.oSettings.gWidth * 2))
						backgroundColor: c.oSettings.pColor
						opacity: c.oSettings.pOpacity

					g++

		c.setupWindow()
		c.createGrid()
) jQuery