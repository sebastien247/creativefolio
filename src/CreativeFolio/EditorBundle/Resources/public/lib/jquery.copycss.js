(function($) {

	$.fn.copyCSS = function( style, toNode ){
		var self = $(this),
		styleObj = {},
		has_toNode = typeof toNode != 'undefined' ? true: false;
		if( !$.isArray( style ) ) {
			style=style.split(' ');
		}
		$.each( style, function( i, name ){ 
			if(has_toNode) {
				toNode.css( name, self.css(name) );
			} else {
				styleObj[name] = self.css(name);
			}
		});
		return ( has_toNode ? self : styleObj );
	}
})(jQuery);