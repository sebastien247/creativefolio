jQuery.noConflict()
jQuery(document).ready ($) ->
	##################
	# Load url

	class Ajax
		constructor: (@url) ->

		display: ->
			$.ajax(
				url: @url
			).done (html) ->
				$(".ui-layout-west").empty().append html

	##################
	# Popup

	class Poplight
		constructor: (@url) ->

		display: ->
			alert("POPUUUUUUP")


	##################
	# Charge l'url dans le menu de gauche

	$('a.load_to_left').bind 'click', (event) =>
		event.preventDefault()
		target = $(event.currentTarget)
		url = target.attr('href') 
		load = new Ajax url
		load.display()

	##################
	# Layout.Jquery
	$(document).ready ->
		outerLayout = $("body").layout
			west__size: 250
			west__maxSize: 450
			west__spacing_open: 3
			west__resizerCursor: "col-resize"
			north__resizable: false
			north__closable: false
			north__spacing_open: 0
			onresize: ->
				#$(window).trigger "resize.global"
		
		#outerLayout = $("body").layout()

	###################
	icons =
		header: "ui-icon-circle-arrow-e"
		activeHeader: "ui-icon-circle-arrow-s"

	$( "#accordion" ).accordion
		collapsible: true
		icons: icons
		active: false

	$("#toggle").button().click ->
		if $("#accordion").accordion("option", "icons")
			$("#accordion").accordion "option", "icons", null
		else
			$("#accordion").accordion "option", "icons", icons
