jQuery.noConflict()
jQuery(document).ready ($) ->
	class bloc
		container_style = undefined
		constructor: (item)->
			@item = item

			@container_style = container_style
			@container_style = document.createElement("div") unless container_style
			@container_style.className = 	"configuration_popup"

		save: ->
			$.ajax(
				url: "http://www.google.com"
			).done (html) ->
				logs(html)

		config: ->
			poplight = document.createElement("div")
			poplight.className = "fancybox"
			$(poplight).append "FANCYBOX - CONFIGURATION DU BLOC"

		popup: (id)->

			$(@container_style).margin(
				label: "Marge : "
				min: 0
				max: 400
				directions: "all"
				valueDefault: 20
				item: @item
				labelLeft: "Marge gauche : "
				labelTop: "Marge haut : "
				labelBottom: "Marge bas : "
				labelRight: "Marge droite : "
				valueDefaultTop: 10
				valueDefaultLeft: 5
				valueDefaultBottom: 250
				valueDefaultRight: 80
			).backgroundColor(
				color: "#FF0055"
			).padding(
				label: "Marge : "
				min: 0
				max: 400
				directions: "all"
				valueDefault: 20
				item: @item
				labelLeft: "Remplissage gauche : "
				labelTop: "Remplissage haut : "
				labelBottom: "Remplissage bas : "
				labelRight: "Remplissage droite : "
				valueDefaultTop: 10
				valueDefaultLeft: 5
				valueDefaultBottom: 250
				valueDefaultRight: 80
			).dialog
				autoOpen: false
				title: "Configurations du bloc"
				create: (event, ui ) ->
					$(this).attr "data-id",id
					#console.log $(this)
				close: (event, ui ) ->
					console.log "CLOSSSEEEEE"

			#addStyles = new styles (@item)
			#addStyles.margin(300)
			#addStyles.float()
			#addStyles.display()


	class styles
		container_style = undefined
		constructor: (item) ->
			@item = item

			@container_style = container_style
			@container_style = document.createElement("div") unless container_style
			@container_style.className = 	"configuration_popup"
			$(@container_style).append "CONFIGURATION DU BLOC"

		margin: (max) ->
			@max = 250 unless max

			###$(@container_style).margin
				label: "Marge : "
				min: 0
				max: 400
				directions: "all"
				valueDefault: 20
				item: @item
				labelLeft: "Marge gauche : "
				labelTop: "Marge haut : "
				labelBottom: "Marge bas : "
				labelRight: "Marge droite : "
				valueDefaultTop: 10
				valueDefaultLeft: 5
				valueDefaultBottom: 250
				valueDefaultRight: 80
			###

			
			marginTop_Label = document.createElement("div")
			$(marginTop_Label).append "Marge du haut : "
			marginTop = document.createElement("div")
			marginTop.className = "config-margin-top slider"

			marginBottom_Label = document.createElement("div")
			$(marginBottom_Label).append "Marge du bas : "
			marginBottom = document.createElement("div")
			marginBottom.className = "config-margin-bottom slider"

			marginLeft_Label = document.createElement("div")
			$(marginLeft_Label).append "Marge de gauche : "
			marginLeft = document.createElement("div")
			marginLeft.className = "config-margin-left slider"

			marginRight_Label = document.createElement("div")
			$(marginRight_Label).append "Marge de droite : "
			marginRight = document.createElement("div")
			marginRight.className = "config-margin-right slider"

			$(@container_style).append marginTop_Label
			$(@container_style).append marginTop
			$(@container_style).append marginBottom_Label
			$(@container_style).append marginBottom
			$(@container_style).append marginLeft_Label
			$(@container_style).append marginLeft
			$(@container_style).append marginRight_Label
			$(@container_style).append marginRight

			item = @item

			$([marginTop, marginBottom, marginLeft, marginRight]).each (idx, elementx) ->
				$(this).append '<div><input type="text" id="value" size="4" disabled/></div>'

				_margin_direction = "margin-top" if marginTop is elementx
				_margin_direction = "margin-bottom" if marginBottom is elementx
				_margin_direction = "margin-left" if marginLeft is elementx
				_margin_direction = "margin-right" if marginRight is elementx

				$(this).slider
					value: 0
					orientation: "horizontal"
					range: "min"
					animate: true
					min: 0
					max: max
					slide: (event, ui) ->
						$("input#value", this).val( ui.value );
					change: (event, ui) ->
						console.log ui.value
						item.css _margin_direction, ui.value
			
		padding: ->



		float: ->
			floatLeft_Label = document.createElement("div")
			$(floatLeft_Label).append "Faire flotter gauche : "
			floatLeft = document.createElement("div")
			floatLeft.className = "config-float-left checkbox"

			floatRight_Label = document.createElement("div")
			$(floatRight_Label).append "Faire flotter droite : "
			floatRight = document.createElement("div")
			floatRight.className = "config-float-right checkbox"

			$(@container_style).append floatLeft
			$(@container_style).append floatRight

			item = @item

			$([floatLeft, floatRight]).each (idx, elementx) ->
				cb = document.createElement('input')
				cb.value = "right"  if floatRight is elementx
				cb.value = "left"  if floatLeft is elementx
				cb.type = "radio"
				cb.id = "value"
				cb.name = "float"
				$(this).append cb
				$(this).on "click", (event, ui) ->
					console.log cb.value
					console.log cb
					console.log this.value
					item.css "float", cb.value




			#faire flotter

		display: (title) ->
			@title = title unless "Configuration"
			divTitle = document.createElement("div")
			$(divTitle).append @title
			$(@container_style).append divTitle

			document.body.appendChild @container_style

			$(@container_style).dialog(
				width: 400
				height: 200
			).trigger "click"


#############################


	##################
	## hugrid ##
	###
	$(document).ready ->
		definegrid = ->
			pageUnits = "%"
			colUnits = "px"
			pagewidth = 100
			columns = 6
			columnwidth = 140
			gutterwidth = 24
			pagetopmargin = 35
			rowheight = 20
			gridonload = "on"
			container = $(".ui-layout-center")
			makehugrid()

		definegrid()
		setgridonload()

	$(window).resize ->
		definegrid()
		setgridonresize()

	$(".ui-layout-center").resize ->
		definegrid()
		setgridonresize()
	###


	gOverride =
		gColor: '#FF0000'
		gColumns: 12
		gOpacity: 0.35
		gWidth: 10
		pColor: '#C0C0C0'
		pHeight: 15
		pOffset: 0
		pOpacity: 0.55
		center: false
		gEnabled: false
		pEnabled: false
		setupEnabled: true
		fixFlash: false
		size: 960
		showGridTo: '.ui-layout-center'
		showSetupTo: '.ui-layout-west'

	$('.ui-layout-center').Grid(gOverride)

	# CONFIGURATION

	dropped = false
	draggable_sibling = undefined
	#snap_grid = 
	bloc_structure = "#bloc_structure"
	bloc_structure_dropped_selected = ".selected"
	container = "#container"
	ai = 0 


	###
	# Charge les blocs et style enregistré dans la Base De Donnée
	#
	###
	$.getJSON window.location.origin+""+window.location.pathname+"style/get/blocs/json", (json) ->

		obj = $.parseJSON( json )

		window.style = obj # VARIABLE GLOBAL !!!!!!!!

		console.log obj

		$.each obj.blocs, (index, obj) ->
			dynBloc = $("<div/>",
				#"data-id": "bloc_"+obj.id
				"data-id": obj.id
				css: obj.style
				text: obj.poid
			).appendTo(container).resizable().addClass('dropped').css("cursor", "move")

			popupCssAction dynBloc

			interractionInitAction dynBloc
			console.log "{{ path('welcome') }}"
			$("#loader").fadeOut "slow"

	$("#save-site-btn").on "click", () ->
		updateAllProperties window.style

	###
	# Initialisation des interractions a la creation d'un bloc
	###
	interractionInitAction = (obj) ->
		#$([ui.item], elementx).each (idx, elementx) ->
		$(obj).each (idx, elementx) ->
			$(elementx).resizable(
				helper: "ui-resizable-helper"
				create: (event, ui) ->
					console.log "resizable create"

				start: (event, ui) ->
					console.log "resizable start"

				resize: (event, ui) ->
					console.log "resizable resize"

				stop: (event, ui) ->
					console.log "resizable stop"
			).on("click", (event) ->
				console.log "selected"
				$(elementx).toggleClass('selected')

				popupCssAction this

				$(elementx).sortable()
			)
		return true

	###
	# Update des styles
	###
	updateStyleAction = (blocsJson) ->
		$.ajax
			url: window.location.origin+""+window.location.pathname+"style/update"
			type: "POST"
			data: {"creativefolio_editorbundle_styletype[css]": JSON.stringify(blocsJson), "creativefolio_editorbundle_styletype[css_saved]": JSON.stringify(blocsJson),}
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Style]"

			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Style] : "+errorThrown

	###
	# Update l'ordre des blocs
	###
	updateOrderAction = (tabObj) ->
		$.ajax
			url: window.location.origin+""+window.location.pathname+"style/update/order"
			type: "POST"
			data: {"creativefolio_editorbundle_styletype[css]": tabObj, "creativefolio_editorbundle_styletype[css_saved]": tabObj,}
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Bloc Order]"

			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Bloc Order] : "+errorThrown

	updateAllProperties = (object) ->
		# Update de l'ordre des Blocs dans : window.style
		order = $(container).sortable('toArray',{attribute:'data-id'});
		console.log order

		window.tmp = new Array()

		$(order).each (idx, elementx) ->
			$(window.style.blocs).each (idy,elementy) ->
				window.tmp.push(elementy) if parseInt(elementx) is parseInt(elementy.id)

		window.style.blocs = window.tmp

		window.tmp = null

		# Sauvegarde des Styles et du poid
		$(window.style.blocs).each (idx, elementx) ->
			# Copy les propriété css
			obloc = $('div[data-id="'+elementx.id+'"]')
			elementx.style = obloc.copyCSS('width height float');
			elementx.poid = obloc.index()
			



		$.ajax
			url: window.location.origin+""+window.location.pathname+"style/update"
			type: "POST"
			data: {"creativefolio_editorbundle_styletype[css]": object, "creativefolio_editorbundle_styletype[css_saved]": object,}
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Bloc Order]"

			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Bloc Order] : "+errorThrown
			complete: (jqXHR, textStatus) ->
				alert "Sauvegarde réussie avec succes" if textStatus = "success"

	deleteBloc = (object) ->
		alert "ok"

	###
	#	PopUp des propriété css
	###
	popupCssAction = (obj) ->
		objId = $(obj).data("id")
		popup = $('.configuration_popup[data-id='+objId+']')

		unless popup.data("id")?
			# on creer le popup
			bloc_config = new bloc obj
			bloc_config.config()
			bloc_config.popup( objId )

		else
			# on affiche le popup
			$(popup).dialog("open")

	###	
	# Ferme tout les popup si le clic est en dehor de la zone des popup et d'un element dropper
	#
	# Close Pop-in If the user clicks anywhere else on the page
	# set for html for jsfiddle, but should be 'body'
	###
	$("body").on "click", (e) -> 
		$(".configuration_popup").dialog "close"  if $("#dialog").dialog("isOpen") and not $(e.target).is(".ui-dialog, a") and not $(e.target).is(".dropped") and not $(e.target).closest(".ui-dialog").length

	###
	# DRAGGABLE
	# Definie les elements draggable lié au sortable du container du cyberfolio
	###
	#$([bloc_structure, bloc_structure_dropped_selected]).each (idx, elementx) ->
	$(bloc_structure).draggable(
		connectToSortable: container
		revert: "invalid"
		addClasses: true
		distance: 10 # Le drag ne commence qu'à partir de 10px de distance de l'élément
		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		helper: ->
			console.log "draggable helper"
			$(bloc_structure).clone().appendTo("body").css(
				zIndex: 5
				height: "50px"
				width: "50px"
				float: "left"
			).show()
	).disableSelection()
				

	# SORTABLE

	#$( [ container, bloc_structure_dropped_selected ] ).each (idx, elementax) ->
	$(container).sortable(
		#connectWith: $( "#container")
		revert: true
		addClasses: true
		placeholder: "ui-sortable-placeholder"

		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		#helper: "clone"
		helper: (event, ui) ->
			console.log "sortable helper"
			$(ui).clone().appendTo("body").css("zIndex", 5).show()

		activate: (event, ui) ->
			console.log "sortable activate"

		change: (event, ui) ->
			console.log "sortable change"

		beforeStop: (event, ui) ->
			console.log "sortable beforeStop"

		receive: (event, ui) ->
			console.log "sortable receive"

		deactivate: (event, ui) ->
			console.log "sortable deactivate"

		out: (event, ui) ->
			console.log "sortable out"

		update: (event, ui) ->
			console.log "sortable update"

			$(ui.item).addClass 'dropped'

			console.log "new index : "
			console.log ui.item.index()

			# si il n'y a pas d'id dans le bloc on l'ajoute a la BDD puis on initialise le propriété CSS....
			unless $(ui.item).attr "data-id"
				$.ajax
					url: window.location.origin+""+window.location.pathname+"bloc/create"
					type: "POST"
					data: {"creativefolio_editorbundle_bloctype[nom]": "Mon bloc", "creativefolio_editorbundle_bloctype[poid]": ui.item.index() }

					beforeSend: () ->
						# Attribut un id unique
						blocId = new Date().getTime()
						#$(ui.item).attr "data-id","bloc_"+blocId
						$(ui.item).attr "data-id",blocId

						# Initialisation des propriété CSS par defaut
						ui.item.css
							float: ui.placeholder.context.style.float
							height: ui.placeholder.context.style.height
							width: ui.placeholder.context.style.width
							cursor: "move"

					success: (data, textStatus, xhr) ->

						### Enregistre le bloc dans la BDD : Peux etre utile pour l'attribution de vidéos, images, 3D...
						# Recupere l'id dans data-id et la convertie en integer
						blocId = parseInt($(ui.item).attr("data-id").replace("bloc_",""))

						# Copy les propriété css
						CSS = ui.item.copyCSS('width height float');
						blocsJson = new Object()
						blocsJson.id = blocId
						blocsJson.poid = ui.item.index()
						blocsJson.style = CSS

						# Envoie les propriété css dans la BDD
						updateStyleAction blocsJson
						###

						# Recupere l'id dans data-id et la convertie en integer
						blocId = parseInt($(ui.item).attr("data-id").replace("bloc_",""))

						# Copy les propriété css
						CSS = ui.item.copyCSS('width height float');
						oBloc = new Object()
						oBloc.id = blocId
						oBloc.poid = ui.item.index()
						oBloc.style = CSS

						# Ajoute le bloc dans l'objet Global
						window.style.blocs.push oBloc

						# Applique les interraction utilisateur
						interractionInitAction ui.item
 
						# Ouvre les popup de personnalisation des propriété css
						popupCssAction ui.item

					error: (xhr, textStatus, errorThrown) ->
						alert "Erreur [Bloc] : "+errorThrown

					complete: () ->


		start: (event, ui) ->
			console.log "sortable start"


			#$(ui.item).toggleClass('dropped')
			draggable_sibling = $(ui.item).prev()

			# Configuration du block par default
			###ui.item.height("50px")
			ui.item.width("50px")
			ui.item.css "float", "left"###

			# Configuration du placeholder
			ui.placeholder.height( ui.helper.height() );
			ui.placeholder.width( ui.helper.width() );
			#$(ui.placeholder) = $(ui.helper).clone().show()
			#ui.placeholder.css("float", ui.item.css "float") # <------ Ne fonctionne pas

			$(ui.placeholder).css
				backgroundColor: "#FF0"
				float: "left"

			# Configuration du helper
			#ui.helper.height( ui.item.height() );
			#ui.helper.width( ui.item.width() );

			###ui.item.css
				float: "left"
				width: "50px"
				height: "50px"
			###


		stop: (event, ui) ->
			console.log "sortable stop"
			#console.log ui.item

			draggable_sibling = $(ui.item).prev()

			console.log ui
	).disableSelection()



	$("#trash").droppable
	  
		# Lorsque l'on passe un élément au dessus de la poubelle
		over: (event, ui) ->
		
			# On ajoute la classe "hover" au div .trash
			$(this).addClass "hover"

			# On cache l'élément déplacé
			ui.draggable.hide()
			
			# On indique via un petit message si l'on veut bien supprimer cet élément
			#$(this).text "Remove " + ui.draggable.find(".item").text()
			
			# On change le curseur
			$(this).css "cursor", "pointer"

	 	# Lorsque l'on quitte la poubelle
		out: (event, ui) ->
			
			# On retire la classe "hover" au div .trash
			$(this).removeClass "hover"
			
			# On réaffiche l'élément déplacé
			#ui.draggable.show()
			
			# On remet le texte par défaut
			#$(this).text "Trash"
			
			# Ainsi que le curseur par défaut
			$(this).css "cursor", "normal"

	  
		# Lorsque l'on relache un élément sur la poubelle
		drop: (event, ui) ->
			
			# On retire la classe "hover" associée au div .trash
			$(this).removeClass "hover"
			
			# On ajoute la classe "deleted" au div .trash pour signifier que l'élément a bien été supprimé
			$(this).addClass "deleted"
			
			# On affiche un petit message "Cet élément a été supprimé" en récupérant la valeur textuelle de l'élément relaché
			#$(this).text ui.draggable.find(".item").text() + " removed !"
			
			# On supprimer l'élément de la page, le setTimeout est un fix pour IE (http://dev.jqueryui.com/ticket/4088)
			#setTimeout (->
			ui.draggable.remove()
			ui.helper.remove()
			#), 1
			
			# On retourne à l'état originel de la poubelle après 2000 ms soit 2 secondes
			elt = $(this)
			setTimeout (->
				elt.removeClass "deleted"
				#elt.text "Trash"
			), 2000


	###
	# Configuration de la grille
	#
	###
	$("#config-grid-btn").on "click", (event) ->
		$.ajax(
			url: "./config_grid"
		).done (html) ->
			$(html).dialog
				height: 400
				width: 400
				modal: true
				buttons:
					"Envoyer": ->
						$("#monid form").ajaxSubmit
							url: submitcat
							type: "post"
							error: ->
								alert "theres an error with AJAX"

							beforeSubmit: ->

							success: (e) ->
								$(this).dialog "close"

					Cancel: ->
						$(this).dialog "close"







