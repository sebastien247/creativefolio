jQuery(document).ready ($) ->

	##############################################################
	# CONFIGURATION
	#
	##############################################################

	dropped = false
	draggable_sibling = undefined
	#snap_grid = 
	bloc_structure = "#bloc_structure"
	bloc_structure_menu = "#bloc_structure_menu"
	bloc_structure_dropped_selected = ".selected"
	bloc_project = "#blocProject"
	bloc_structure_of_project = ".blocSortableOfProject"
	#container = "#container"
	container = ".pageOfCyberfolio"
	ai = 0

	##############################################################
	# Initialisation des grilles modulaire
	#
	##############################################################
	gOverride =
		gColor: '#FF0000'
		gColumns: 12
		gOpacity: 0.35
		gWidth: 10
		pColor: '#C0C0C0'
		pHeight: 15
		pOffset: 0
		pOpacity: 0.55
		center: false
		gEnabled: false
		pEnabled: false
		setupEnabled: true
		fixFlash: false
		size: 960
		showGridTo: '.ui-layout-center'
		showSetupTo: '.ui-layout-west'

	$('.ui-layout-center').Grid(gOverride)

	##############################################################
	# Charge les blocs et style enregistré dans la Base De Donnée
	#
	##############################################################
	loadCyberfolio = ->
		$.getJSON window.location.origin+""+window.location.pathname+"style/get/blocs/json", (json) ->
			console.log "getjson"

			obj = $.parseJSON( json )

			window.cyberfolio = obj # VARIABLE GLOBAL !!!!!!!!

			unless obj.blocs
				obj.blocs = Array()
				window.cyberfolio = obj
				console.log obj
			unless obj.pages
				obj.pages = new Array()
				window.cyberfolio = obj
				#obj.pages = [{id:1,name:"home"}]
				console.log obj

			console.log obj.global.style
			console.log "globallllll"

			# Ajout des pages
			$.each obj.pages, (index, obj) ->
				createPage(obj.name,obj.id);

				# Ajout des blocs dans les pages
				$(obj.blocsId).each (index, blocId) ->
					console.log blocId
					console.log "blocId"

					$.each window.cyberfolio.blocs, (index, objBloc) ->
						if parseInt(blocId) is parseInt(objBloc.id)
							console.log "eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"
							dynBloc = $("<div/>",
								#"data-id": "bloc_"+obj.id
								"data-id": objBloc.id
								css: objBloc.style
								#text: obj.poid
							)
							dynBloc.html "<div id='bloc_content'>"+objBloc.content+"</div>" if objBloc.content

							$("#"+obj.id+".pageOfCyberfolio","#allPageOfCyberfolio").append(dynBloc)
							dynBloc.resizable().addClass('dropped').css("cursor", "move")
							#dynBloc.addClass('dropped')
							
							$("#"+obj.id+".pageOfCyberfolio","#allPageOfCyberfolio").css
								backgroundImage: "none"

							popupCssAction dynBloc

							interractionInitAction dynBloc
							console.log "{{ path('welcome') }}"

			formGlobalStyle()
			loadProjectStructure()
			endLoader()

	loadCyberfolio()


	##############################################################
	# Route avec BackboneJS
	#
	##############################################################

	app = {}

	app.Router = Backbone.Router.extend(routes:
		"" : "structureRoute"
		"structure": "structureRoute" # matches http://example.com/#structure
		"projet": "projetRoute"
		"style": "styleRoute"
		"page": "pageRoute"
		"reglage": "reglageRoute"
		"*actions": "structureRoute" # matches http://example.com/#anything-here
	)

	##############################################################
	# Initialisation des Routes Backbone
	# 
	##############################################################
	app.router = new app.Router

	app.router.on "route:structureRoute", (actions) ->
		$("#allPageOfCyberfolioForProject").hide()
		$("#allPageOfCyberfolio").show()
		$(allPages).each (idx,elementx) ->
			elementx.hide()
		structure.show()

	app.router.on "route:projetRoute", () ->
		$("#allPageOfCyberfolioForProject").show()
		$("#allPageOfCyberfolio").hide()
		loadProjectStructure()
		$(allPages).each (idx,elementx) ->
			elementx.hide()
		projet.show()

	app.router.on "route:styleRoute", () ->
		$("#allPageOfCyberfolioForProject").hide()
		$("#allPageOfCyberfolio").show()
		$(allPages).each (idx,elementx) ->
			elementx.hide()
		style.show()

	app.router.on "route:pageRoute", () ->
		$("#allPageOfCyberfolioForProject").hide()
		$("#allPageOfCyberfolio").show()
		$(allPages).each (idx,elementx) ->
			elementx.hide()
		page.show()

	app.router.on "route:reglageRoute", () ->
		$("#allPageOfCyberfolioForProject").hide()
		$("#allPageOfCyberfolio").show()
		$(allPages).each (idx,elementx) ->
			elementx.hide()
		reglage.show()

	##############################################################
	# Chargement fini, Affichage du site
	#
	##############################################################
	endLoader = ->
		$("#loader").fadeOut "slow"

	##############################################################
	# Chargement de Layout.Jquery
	#
	##############################################################
	outerLayout = $("body").layout
		west__size: 250
		west__maxSize: 450
		west__spacing_open: 3
		west__resizerCursor: "col-resize"
		north__resizable: false
		north__closable: false
		north__spacing_open: 0
		onresize: ->
			#$(window).trigger "resize.global"
	#outerLayout = $("body").layout()

	##############################################################
	# Réecriture des liens du menu pour BackboneJS
	#
	##############################################################
	$('a.load_to_left').each (idx, elementx) ->
		console.log $(elementx).attr('href')
		url = $(elementx).attr('href').replace('editor/', 'editor/#')
		$(elementx).attr('href', url)


	##############################################################
	# Evenement : Réecriture des liens du menu pour BackboneJS
	#
	##############################################################
	$('#publier').on "click", (event) ->
		updateAllProperties(window.cyberfolio, true)

	##############################################################
	# Evenement : Création d'une page
	##############################################################
	$('#page').on "submit", ".add", (event) ->
		event.preventDefault()
		namePage = $("input#nom",this).val()
		idPage = new Date().getTime()

		createPage(namePage, idPage)
		createPageForProject(namePage, idPage)

		$(".dropped #bloc_content #menu").append "<li id='"+idPage+"'><a href='"+idPage+"_"+namePage+".html'>"+namePage+"</a></li>"

	##############################################################
	# Fonction : Création des pages et liens
	##############################################################
	createPage = (namePage, idPage) ->
		if parseInt(idPage) isnt 0x1
			# Ajoute la page dans la liste de la section page
			li = $("<li><p>"+namePage+"</p><a class='delete' id='"+idPage+"'href='#'>Supprimmer</a></li>")
			$('#listPage').append(li)

			# Ajoute la page dans la liste de la section structure
			li = $("<li><a class='page' id='"+idPage+"'href='#'>"+namePage+"</a></li>")
			$('#listPageStructure').append(li)

			# Ajoute le container des bloc dans la page central de la structure
			div = $("<div id='"+idPage+"' class='pageOfCyberfolio' data-name='"+namePage+"'></div>")

			# Appliqye les css global
			globalCSS = saveCss($(".pageOfCyberfolio:first"))
			$(div).css globalCSS

			$(div).css
				width: "960px"
				height: "100%"
			$(container).last().after(div)
			$(div).css
				top: "-999999px"
				left: "-999999px"
			$(div).sortable sortableOfPageOfCyberfolio
			$(div).disableSelection()

			# Ajoute le lien dans le menu
			liMenu = $("<li id='"+idPage+"'><a href='"+idPage+"_"+namePage+".html'>"+namePage+"</a></li>")
			$("#bloc_content ul",bloc_structure_menu).append(liMenu)
		else
			# Ajoute la page dans la liste de la section structure
			li = $("<li><a class='page' id='"+idPage+"'href='#'>"+namePage+"</a></li>")
			$('#listPageStructure').append(li)

	##############################################################
	# Fonction : Création des pages et liens pour la structure des projects
	##############################################################
	createPageForProject = (namePage, idPage) ->
		if parseInt(idPage) isnt 0x1
			# Ajoute la page dans la liste de la section project
			li = $("<li><a class='page' id='"+idPage+"'href='#'>"+namePage+"</a></li>")
			$('#listPageStructureForProject').append(li)

			# Ajoute le container des bloc dans la page central de la structure des projets
			div = $("<div id='"+idPage+"' class='pageOfCyberfolioForProject' data-name='"+namePage+"'></div>")

			# Appliqye les css global
			globalCSS = saveCss($(".pageOfCyberfolio:first"))
			$(div).css globalCSS

			$(div).css
				width: "960px"
				height: "100%"

			#$(".pageOfCyberfolioForProject").last().after(div)
			$(div).insertAfter(".pageOfCyberfolioForProject:last")

			$(div).css
				top: "-999999px"
				left: "-999999px"

			#$(div).sortable sortableOfPageOfCyberfolio
			$(div).disableSelection()
		else
			# Ajoute la page dans la liste de la section project
			li = $("<li><a class='page' id='"+idPage+"'href='#'>"+namePage+"</a></li>")
			$('#listPageStructureForProject').append(li)

	##############################################################
	# Evenement : Affiche et masque les container des pages Structure
	#
	##############################################################
	$('#listPageStructure').on "click", "li a.page", (event) ->
		event.preventDefault()

		$(".pageOfCyberfolio","#allPageOfCyberfolio").each (idx,elementx) ->
			$(elementx).css
				top: "-999999px"
				left: "-999999px"

		console.log $("#"+$(this).attr("id"),"#allPageOfCyberfolio")
		
		$("#"+$(this).attr("id"),"#allPageOfCyberfolio").css
			top: "18px"
			left: "0px"

	##############################################################
	# Evenement : Affiche et masque les container des pages Project
	#
	##############################################################
	$('#listPageStructureForProject').on "click", "li a.page", (event) ->
		event.preventDefault()

		# Masque tout
		$(".pageOfCyberfolioForProject","#allPageOfCyberfolioForProject").each (idx,elementx) ->
			$(elementx).css
				top: "-999999px"
				left: "-999999px"

		console.log $("div#"+$(this).attr("id")+".pageOfCyberfolioForProject")
		
		# Affiche la page ciblé
		$("div#"+$(this).attr("id")+".pageOfCyberfolioForProject").css
			top: "18px"
			left: "0px"

	##############################################################
	# Evennement : suppression d'une page
	#
	##############################################################
	$('#page').on "click", ".delete", (event) ->
		event.preventDefault()
		$(this).parent().remove()
		$("#"+$(this).attr("id"),"#allPageOfCyberfolio").remove()
		$("#"+$(this).attr("id"),"#allPageOfCyberfolioForProject").remove()
		$("#"+$(this).attr("id"),"#listPageStructure").parent().remove()
		$("#"+$(this).attr("id"),"#listPageStructureForProject").parent().remove()
		$(".dropped #bloc_content #menu #"+$(this).attr('id')).remove()
		$("#bloc_structure_menu #bloc_content #menu #"+$(this).attr('id')).remove()

	##############################################################
	# Fonction : Génère les fichiers du cyberfolio
	#
	##############################################################
	publierAction = () ->
		url = $('#publier').attr('href')
		$.ajax
			url: url
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Publier]"

			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Bloc Order] : "+errorThrown
			complete: (jqXHR, textStatus) ->
				alert "Votre cyberfolio est maintenant en ligne" if textStatus = "success"

	##############################################################
	# Fonction : Serialize toute les propriétés css listé
	#
	##############################################################
	saveCss = (obj) ->
		css = obj.copyCSS('width height bottom left right top float margin margin-right margin-bottom margin-left margin-top padding padding-top padding-left padding-right padding-bottom background-color color font font-family font-size font-stretch font-style font-weight text-align text-justify text-shadow text-transform opacity border-bottom border-top border-left border-right border-color border-style border-shadow border-radius border-width border-top-color border-top-style border-top-shadow border-top-radius border-top-width border-bottom-color border-bottom-style border-bottom-shadow border-bottom-radius border-bottom-width border-right-color border-right-style border-right-shadow border-right-radius border-right-width border-left-color border-left-style border-left-shadow border-left-radius border-left-width');
		return css

	##############################################################
	# Fonction : Sauvegarde tout le cyberfolio dans la base de données
	# Save prorieté css des blocs
	# Save prorieté css global
	# Save l'ordre de blocs
	# Save les pages
	# Save to JSON in window.cyberfolio
	#
	##############################################################

	updateAllPropertiesToGlobal = (object) ->
		# Ordre des Blocs
		###
		order = $(container).sortable('toArray',{attribute:'data-id'});
		console.log order
		console.log $(container)
		###

		unless window.cyberfolio
			document.location.href = "#structure"
			return true

		# Save toute les pages
		console.log container
		window.cyberfolio.pages = new Array()
		$(container).each (idx, elementx) ->
			console.log this
			console.log "tttoootto"
			oPage = new Object()
			oPage.id = elementx.id
			oPage.name = $(elementx).data "name"
			window.cyberfolio.pages.push(oPage)

		# Save les styles global
		window.cyberfolio.global = new Object()
		window.cyberfolio.global.style = saveCss($(".pageOfCyberfolio:first"))

		# Associe les blocs au pages
		$(container).each (idx, elementx) ->
			orderBlocs = $(elementx).sortable('toArray',{attribute:'data-id'});
			$(window.cyberfolio.pages).each (idy,elementy) ->
				elementy.blocsId = orderBlocs if parseInt(elementx.id) is parseInt(elementy.id)


		# Save tout les blocs
		console.log container
		window.cyberfolio.blocs = new Array()
		$(".dropped","#allPageOfCyberfolio").each (idx, elementx) ->
			console.log this
			obloc = new Object()
			obloc.id = $(elementx).data "id"
			obloc.content = $("#bloc_content",elementx).html()
			obloc.style = saveCss($(elementx))
			window.cyberfolio.blocs.push(obloc)


		### Styles des blocs
		$(window.cyberfolio.blocs).each (idx, elementx) ->
			obloc = $('div[data-id="'+elementx.id+'"]')
			elementx.style = saveCss(obloc) # Copy les propriété css du bloc
		###


	updateAllProperties = (object, publier) ->
		updateAllPropertiesToGlobal window.cyberfolio
		# Sauvegarde le tout dans la BDD
		$.ajax
			url: window.location.origin+""+window.location.pathname+"style/update"
			type: "POST"
			data: {"creativefolio_editorbundle_styletype[css]": object, "creativefolio_editorbundle_styletype[css_saved]": object,}
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Bloc Order]"
			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Bloc Order] : "+errorThrown
			complete: (jqXHR, textStatus) ->
				alert "Sauvegarde réussie avec succes" if textStatus = "success"
				publierAction() if publier

	##############################################################
	# Fonction : Affichage des Styles global
	#
	##############################################################
	formGlobalStyle = ->
		globalStyle = $('.pageOfCyberfolio')
		if globalStyle.length is 0
			globalStyle = $("<div/>")
			console.log "global"
		$('#global').formMargin(
			label: "Marge : "
			min: 0
			max: 1000
			directions: "all"
			item: globalStyle
			labelLeft: "Marge gauche : "
			labelTop: "Marge haut : "
			labelBottom: "Marge bas : "
			labelRight: "Marge droite : "
			valueDefaultTop: globalStyle.css("marginTop").replace('px', '')
			valueDefaultLeft: globalStyle.css("marginLeft").replace('px', '')
			valueDefaultBottom: globalStyle.css("marginBottom").replace('px', '')
			valueDefaultRight: globalStyle.css("marginRight").replace('px', '')
		).formBackgroundColor(
			color: globalStyle.css("backgroundColor")
			item: globalStyle
		).formPadding(
			label: "Marge : "
			min: 0
			max: 1000
			directions: "all"
			item: globalStyle
			labelLeft: "Remplissage gauche : "
			labelTop: "Remplissage haut : "
			labelBottom: "Remplissage bas : "
			labelRight: "Remplissage droite : "
			valueDefaultTop: globalStyle.css("paddingTop").replace('px', '')
			valueDefaultLeft: globalStyle.css("paddingLeft").replace('px', '')
			valueDefaultBottom: globalStyle.css("paddingBottom").replace('px', '')
			valueDefaultRight: globalStyle.css("paddingRight").replace('px', '')
		).formBorder(
			style: "solid"
			min: 0
			max: 100
			directions: "all"
			item: globalStyle

			labelLeft: "Bordure gauche : "
			labelTop: "Bordure haut : "
			labelBottom: "Bordure bas : "
			labelRight: "Bordure droite : "
			labelAll: "Toutes les bordures : "

			valueDefaultTopWidth: globalStyle.css("borderTopWidth").replace('px', '')
			valueDefaultLeftWidth: globalStyle.css("borderLeftWidth").replace('px', '')
			valueDefaultBottomWidth: globalStyle.css("borderBottomWidth").replace('px', '')
			valueDefaultRightWidth: globalStyle.css("borderRightWidth").replace('px', '')
			valueDefaultAllWidth: globalStyle.css("borderWidth")

			valueDefaultTopColor: globalStyle.css("borderTopColor")
			valueDefaultLeftColor: globalStyle.css("borderLeftColor")
			valueDefaultBottomColor: globalStyle.css("borderBottomColor")
			valueDefaultRightColor: globalStyle.css("borderRightColor")
			valueDefaultAllColor: globalStyle.css("borderColor")

			valueDefaultTopStyle: globalStyle.css("borderTopStyle")
			valueDefaultLeftStyle: globalStyle.css("borderLeftStyle")
			valueDefaultBottomStyle: globalStyle.css("borderBottomStyle")
			valueDefaultRightStyle: globalStyle.css("borderRightStyle")
			valueDefaultAllStyle: globalStyle.css("borderStyle")
		).formFont(
			fontsFamily: "Arial,Comics sans ms, sans serif;Verdena, Roboto"
			color: "#FF0000"
			item: globalStyle
			labelFont: "Police : "
			textAlign: "left"
			min: 5
			max: 50
		).formWidth(
			item: globalStyle
			labelWidth: "Largeur : "
			defaultValue: globalStyle.css("width")
			min: 0
			max: 900
		).formHeight(
			item: globalStyle
			labelHeight: "Hauteur : "
			defaultValue: globalStyle.css("height")
			min: 0
			max: 9000
		).formPosition(
			label: "Position : "
			min: 0
			max: 900
			directions: "all"
			item: globalStyle
			labelLeft: "Position gauche : "
			labelTop: "Position haut : "
			labelBottom: "Position bas : "
			labelRight: "Position droite : "
			valueDefaultTop: globalStyle.css("top")
			valueDefaultLeft: globalStyle.css("left")
			valueDefaultBottom: globalStyle.css("bottom")
			valueDefaultRight: globalStyle.css("right")
		).formBackgroundColor(
			labelBackground: "Couleur de fond : "
			color: globalStyle.css("backgroundColor")
			item: globalStyle
		).formOpacity(
			labelOpacity: "Opacite du bloc : "
			min: 0
			max: 10
			defaultValue: 10
			item: globalStyle
		)
		globalStyle = $('.dropped')

	##############################################################
	# Class : Bloc
	#
	##############################################################
	class bloc
		container_style = undefined
		constructor: (item)->
			@item = item

			@container_style = container_style
			@container_style = document.createElement("div") unless container_style
			@container_style.className = "configuration_popup"

		config: ->
			poplight = document.createElement("div")
			poplight.className = "fancybox"
			$(poplight).append "FANCYBOX - CONFIGURATION DU BLOC"

		popup: (id)->
			$(@container_style).formMargin(
				label: "Marge : "
				min: 0
				max: 1000
				directions: "all"
				item: @item
				labelLeft: "Marge gauche : "
				labelTop: "Marge haut : "
				labelBottom: "Marge bas : "
				labelRight: "Marge droite : "
				valueDefaultTop: @item.css("marginTop").replace('px', '')
				valueDefaultLeft: @item.css("marginLeft").replace('px', '')
				valueDefaultBottom: @item.css("marginBottom").replace('px', '')
				valueDefaultRight: @item.css("marginRight").replace('px', '')
			).formPadding(
				label: "Marge : "
				min: 0
				max: 1000
				directions: "all"
				item: @item
				labelLeft: "Remplissage gauche : "
				labelTop: "Remplissage haut : "
				labelBottom: "Remplissage bas : "
				labelRight: "Remplissage droite : "
				valueDefaultTop: @item.css("paddingTop").replace('px', '')
				valueDefaultLeft: @item.css("paddingLeft").replace('px', '')
				valueDefaultBottom: @item.css("paddingBottom").replace('px', '')
				valueDefaultRight: @item.css("paddingRight").replace('px', '')
			).formBorder(
				style: "solid"
				min: 0
				max: 100
				directions: "all"
				item: @item

				labelLeft: "Bordure gauche : "
				labelTop: "Bordure haut : "
				labelBottom: "Bordure bas : "
				labelRight: "Bordure droite : "
				labelAll: "Toutes les bordures : "

				valueDefaultTopWidth: @item.css("borderTopWidth").replace('px', '')
				valueDefaultLeftWidth: @item.css("borderLeftWidth").replace('px', '')
				valueDefaultBottomWidth: @item.css("borderBottomWidth").replace('px', '')
				valueDefaultRightWidth: @item.css("borderRightWidth").replace('px', '')
				valueDefaultAllWidth: @item.css("borderWidth")

				valueDefaultTopColor: @item.css("borderTopColor")
				valueDefaultLeftColor: @item.css("borderLeftColor")
				valueDefaultBottomColor: @item.css("borderBottomColor")
				valueDefaultRightColor: @item.css("borderRightColor")
				valueDefaultAllColor: @item.css("borderColor")

				valueDefaultTopStyle: @item.css("borderTopStyle")
				valueDefaultLeftStyle: @item.css("borderLeftStyle")
				valueDefaultBottomStyle: @item.css("borderBottomStyle")
				valueDefaultRightStyle: @item.css("borderRightStyle")
				valueDefaultAllStyle: @item.css("borderStyle")
			).formFont(
				fontsFamily: "Arial,Comics sans ms, sans serif;Verdena, Roboto"
				color: @item.css("color")
				item: @item
				labelFont: "Police : "
				textAlign: @item.css("textAlign")
				min: 5
				max: 50
			).formWidth(
				item: @item
				labelWidth: "Largeur : "
				defaultValue: @item.css("width")
				min: 0
				max: 900
			).formHeight(
				item: @item
				labelHeight: "Hauteur : "
				defaultValue: @item.css("height")
				min: 0
				max: 3000
			).formPosition(
				label: "Position : "
				min: 0
				max: 400
				directions: "all"
				item: @item
				labelLeft: "Position gauche : "
				labelTop: "Position haut : "
				labelBottom: "Position bas : "
				labelRight: "Position droite : "
				valueDefaultTop: @item.css("top")
				valueDefaultLeft: @item.css("left")
				valueDefaultBottom: @item.css("bottom")
				valueDefaultRight: @item.css("right")
			).formBackgroundColor(
				labelBackground: "Couleur de fond : "
				color: @item.css("backgroundColor")
				item: @item
			).formOpacity(
				labelOpacity: "Opacite du bloc : "
				min: 0
				max: 10
				defaultValue: 10
				item: @item
			).formContent(
				labelContent: "Ajouter du texte : "
				item: @item
			).formFloat(
				labelFloat: "Float : "
				item: "#bloc"
			).dialog
				height: 400
				width: 400
				autoOpen: false
				title: "Configurations du bloc"
				create: (event, ui ) ->
					$(this).attr "data-id",id
					$("button",this.parentNode).removeClass("ui-button")
					#console.log $(this)
				close: (event, ui ) ->
					console.log "CLOSSSEEEEE"

			#$(@container_style).tabs();



	##############################################################
	# Apparence des icons collaps
	#
	##############################################################
	icons =
		header: "ui-icon-circle-arrow-e"
		activeHeader: "ui-icon-circle-arrow-s"



	##############################################################
	# Accordeon pour les menu dans les popup
	# 
	# A TERMINE !!!!!!!! A charger a la fin du chargement des popup ? faire un bind pour les nouveaux blocs
	#
	##############################################################
	$( "#accordion" ).accordion
		collapsible: true
		icons: icons
		active: false
		heightStyle: "content"
		header: "h3"

	$("#toggle").button().click ->
		if $("#accordion").accordion("option", "icons")
			$("#accordion").accordion "option", "icons", null
		else
			$("#accordion").accordion "option", "icons", icons


	##############################################################
	# Charge les blocs et style enregistré dans la Base De Donnée
	#
	##############################################################
	loadProjectStructure = ->
		# Associe les blocs au pages
		###$(container).each (idx, elementx) ->
			orderBlocs = $(elementx).sortable('toArray',{attribute:'data-id'});
			$(window.cyberfolio.pages).each (idy,elementy) ->
				elementy.blocsId = orderBlocs if parseInt(elementx.id) is parseInt(elementy.id)
		###
		updateAllPropertiesToGlobal window.cyberfolio

		# reinitialisation
		$('#listPageStructureForProject').html ""
		$("#allPageOfCyberfolioForProject").html "<div id='0000000000001' class='pageOfCyberfolioForProject' data-name='home'></div>"

		# Ajout des pages
		$.each window.cyberfolio.pages, (index, obj) ->
			createPageForProject(obj.name,obj.id);
			console.log obj

			# Ajout des blocs dans les pages
			$(obj.blocsId).each (index, blocId) ->

				$.each window.cyberfolio.blocs, (index, objBloc) ->
					if parseInt(blocId) is parseInt(objBloc.id)
						dynBloc = $("<div/>",
							#"data-id": "bloc_"+obj.id
							"data-id": objBloc.id
							css: objBloc.style
							#text: obj.poid
						)
						dynBloc.addClass('dropped blocSortableOfProject')
						dynBloc.sortable()
						dynBloc.html "<div id='bloc_content'>"+objBloc.content+"</div>"# if objBloc.content

						$("#"+obj.id+".pageOfCyberfolioForProject","#allPageOfCyberfolioForProject").append(dynBloc)

						# Sortable
						#dynBloc.sortable()
	updateProjectStructureBloc = ->
		# Associe les blocs au pages
		$(container).each (idx, elementx) ->
			orderBlocs = $(elementx).sortable('toArray',{attribute:'data-id'});
			$(window.cyberfolio.pages).each (idy,elementy) ->
				elementy.blocsId = orderBlocs if parseInt(elementx.id) is parseInt(elementy.id)

		tmp = new Array()

		# Ajout des pages
		$.each window.cyberfolio.pages, (index, obj) ->

			# Ajout des blocs dans les pages
			$(obj.blocsId).each (index, blocId) ->

				$.each window.cyberfolio.blocs, (index, objBloc) ->
					if parseInt(blocId) is parseInt(objBloc.id)
						unless parseInt($("div[data-id="+objBloc.id+"]","#allPageOfCyberfolioForProject").data("id"))
							console.log "totototototototototoototot"
							beforeOfBloc = tmp.pop();
							console.log beforeOfBloc


							dynBloc = $("<div/>",
								#"data-id": "bloc_"+obj.id
								"data-id": objBloc.id
								css: objBloc.style
								#text: obj.poid
							)
							dynBloc.addClass('dropped blocSortableOfProject')
							dynBloc.sortable()
							dynBloc.html "<div id='bloc_content'>"+objBloc.content+"</div>" if objBloc.content
							console.log beforeOfBloc
							#$("div[data-id="+beforeOfBloc+"].dropped","#"+obj.id+".pageOfCyberfolioForProject").after(dynBloc)
							$(dynBloc).insertAfter("#"+obj.id+".pageOfCyberfolioForProject div[data-id="+beforeOfBloc+"].dropped")
							#$(div).insertAfter(".pageOfCyberfolioForProject:last")

						tmp.push objBloc.id


	##############################################################
	# Evennement de sauvegarde
	##############################################################
	$("#save-site-btn").on "click", () ->
		updateAllProperties window.cyberfolio

	##############################################################
	# Initialisation des interractions utilisateur a la creation d'un bloc depuis la base de donnée
	#
	##############################################################
	interractionInitAction = (objForResizable) ->
		#$([ui.item], elementx).each (idx, elementx) ->
		$(objForResizable).each (idx, elementx) ->
			$(elementx).resizable(
				helper: "ui-resizable-helper"
				create: (event, ui) ->
					console.log "resizable create"

				start: (event, ui) ->
					console.log "resizable start"

				resize: (event, ui) ->
					console.log "resizable resize"

				stop: (event, ui) ->
					console.log "resizable stop"
			).on("click", (event) ->
				console.log "selected"
				$(elementx).toggleClass('selected')

				popupCssAction this

				$(elementx).sortable()
			)
		return true

	##############################################################
	# Update des styles dans la base de donné
	# 
	# PLUS NESSAICAIRE !! A SUPPRIMMER !!!
	##############################################################
	updateStyleAction = (blocsJson) ->
		$.ajax
			url: window.location.origin+""+window.location.pathname+"style/update"
			type: "POST"
			data: {"creativefolio_editorbundle_styletype[css]": JSON.stringify(blocsJson), "creativefolio_editorbundle_styletype[css_saved]": JSON.stringify(blocsJson),}
			cache: false
			dataType: "html"
			success: (data, textStatus, xhr) ->
				console.log "Success [Style]"

			error: (xhr, textStatus, errorThrown) ->
				console.log "Erreur [Style] : "+errorThrown

	##############################################################
	#	PopUp des propriété css
	##############################################################
	popupCssAction = (obj) ->
		objId = $(obj).data("id")
		popup = $('.configuration_popup[data-id='+objId+']')

		unless popup.data("id")?
			# on creer le popup
			bloc_config = new bloc obj
			bloc_config.config()
			bloc_config.popup( objId )

		else
			# on affiche le popup
			$(popup).dialog("open")

	##############################################################
	# Evenement de fermeture des popup's
	#
	# Ferme tout les popup si le clic est en dehor de la zone des popup et d'un element dropper
	#
	# Close Pop-in If the user clicks anywhere else on the page
	# set for html for jsfiddle, but should be 'body'
	##############################################################
	$("body").on "click", (e) -> 
		$(".configuration_popup").dialog "close"  if $("#dialog").dialog("isOpen") and not $(e.target).is(".ui-dialog, a") and not $(e.target).is(".dropped") and not $(e.target).is(".colorpicker > *") and not $(e.target).closest(".ui-dialog").length

	##############################################################
	# DRAGGABLE
	# Definie les elements draggable lié au sortable du container du cyberfolio
	#
	##############################################################
	#$([bloc_structure, bloc_structure_dropped_selected]).each (idx, elementx) ->
	$(bloc_structure).draggable(
		connectToSortable: container
		revert: "invalid"
		addClasses: true
		distance: 10 # Le drag ne commence qu'à partir de 10px de distance de l'élément
		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		helper: ->
			console.log "draggable helper"
			$(bloc_structure).clone().appendTo("body").css(
				zIndex: 5
				height: "50px"
				width: "50px"
				float: "left"
			).show()
	).disableSelection()

	##############################################################
	# DRAGGABLE
	# Definie les elements draggable lié au sortable du container du cyberfolio
	#
	##############################################################
	$(bloc_structure_menu).draggable(
		connectToSortable: container
		revert: "invalid"
		addClasses: true
		distance: 10 # Le drag ne commence qu'à partir de 10px de distance de l'élément
		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		helper: ->
			console.log "draggable helper"
			$(bloc_structure_menu).clone().appendTo("body").css(
				zIndex: 5
				height: "50px"
				width: "50px"
				float: "left"
			).show()
	).disableSelection()

	##############################################################
	# DRAGGABLE
	# Definie les elements draggable lié au sortable du container project du cyberfolio
	#
	##############################################################
	$(bloc_project).draggable(
		connectToSortable: bloc_structure_of_project
		revert: "invalid"
		addClasses: true
		distance: 10 # Le drag ne commence qu'à partir de 10px de distance de l'élément
		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		helper: ->
			console.log "draggable helper"
			$(bloc_project).clone().appendTo("body").css(
				zIndex: 5
				height: "50px"
				width: "50px"
				float: "left"
			).show()
	).disableSelection()

	##############################################################
	# SORTABLE
	# Permet la minupulation des blocs
	#
	##############################################################

	#$( [ container, bloc_structure_dropped_selected ] ).each (idx, elementax) ->
	#$(container).sortable(
	sortableOfPageOfCyberfolio =
		connectWith: "#trash"
		revert: true
		addClasses: true
		placeholder: "ui-sortable-placeholder"


		#	use a helper-clone that is append to 'body' so is not 'contained' by a pane
		#helper: "clone"
		helper: (event, ui) ->
			console.log "sortable helper"
			$(ui).clone().appendTo("body").css("zIndex", 5).show()

		activate: (event, ui) ->
			console.log "sortable activate"
			console.log this

		change: (event, ui) ->
			console.log "sortable change"

		beforeStop: (event, ui) ->
			console.log "sortable beforeStop"

		receive: (event, ui) ->
			console.log "sortable receive"

		deactivate: (event, ui) ->
			console.log "sortable deactivate"

		out: (event, ui) ->
			console.log "sortable out"

		update: (event, ui) ->
			console.log "sortable update"

			$(ui.item).addClass 'dropped'

			console.log "new index : "
			console.log ui.item.index()

			# si il n'y a pas d'id dans le bloc on l'ajoute a la BDD puis on initialise le propriété CSS....
			unless $(ui.item).attr "data-id"
				# Attribut un id unique
				blocId = new Date().getTime()
				#$(ui.item).attr "data-id","bloc_"+blocId
				$(ui.item).attr "data-id",blocId
				$(ui.item).addClass 'cyberfolio_bloc'

				# Initialisation des propriété CSS par defaut
				ui.item.css
					float: ui.placeholder.context.style.float
					height: ui.placeholder.context.style.height
					width: ui.placeholder.context.style.width
					cursor: "move"

				# Recupere l'id dans data-id et la convertie en integer
				blocId = parseInt($(ui.item).attr("data-id").replace("bloc_",""))

				# Copy les propriété css
				CSS = saveCss(ui.item)

				oBloc = new Object()
				oBloc.id = blocId
				#oBloc.poid = ui.item.index()
				oBloc.style = CSS

				# Ajoute le bloc dans l'objet Global
				window.cyberfolio.blocs.push oBloc

				# Applique les interraction utilisateur
				interractionInitAction ui.item

				# Ouvre les popup de personnalisation des propriété css
				popupCssAction ui.item

				#updateProjectStructureBloc()

		start: (event, ui) ->
			console.log "sortable start"


			#$(ui.item).toggleClass('dropped')
			draggable_sibling = $(ui.item).prev()

			# Configuration du block par default
			###ui.item.height("50px")
			ui.item.width("50px")
			ui.item.css "float", "left"###

			# Configuration du placeholder
			ui.placeholder.height( ui.helper.height() );
			ui.placeholder.width( ui.helper.width() );
			#$(ui.placeholder) = $(ui.helper).clone().show()
			#ui.placeholder.css("float", ui.item.css "float") # <------ Ne fonctionne pas

			$(ui.placeholder).css
				backgroundColor: "#0694E9"
				float: "left"

			# Configuration du helper
			#ui.helper.height( ui.item.height() );
			#ui.helper.width( ui.item.width() );

			###ui.item.css
				float: "left"
				width: "50px"
				height: "50px"
			###


		stop: (event, ui) ->
			console.log "sortable stop"
			#console.log ui.item
			$(this).css
				backgroundImage: "none"

			draggable_sibling = $(ui.item).prev()

			console.log ui

	$(container).sortable sortableOfPageOfCyberfolio
	$(container).disableSelection()

	##############################################################
	# Poubelle de suppression des blocs
	# Supprime les blocs
	##############################################################
	$("#trash").droppable
		accept: ".dropped"
		tolerance: "pointer"
	  	
		# Lorsque l'on passe un élément au dessus de la poubelle
		over: (event, ui) ->
			
			# On ajoute la classe "hover" au div .trash
			$(this).addClass "hover"

			# On cache l'élément déplacé
			ui.draggable.hide()
			
			# On indique via un petit message si l'on veut bien supprimer cet élément
			#$(this).text "Remove " + ui.draggable.find(".item").text()
			
			# On change le curseur
			$(this).css "cursor", "pointer"

	 	# Lorsque l'on quitte la poubelle
		out: (event, ui) ->
			
			# On retire la classe "hover" au div .trash
			$(this).removeClass "hover"
			
			# On réaffiche l'élément déplacé
			#ui.draggable.show()
			
			# On remet le texte par défaut
			#$(this).text "Trash"
			
			# Ainsi que le curseur par défaut
			$(this).css "cursor", "normal"

	  
		# Lorsque l'on relache un élément sur la poubelle
		drop: (event, ui) ->
			
			# On retire la classe "hover" associée au div .trash
			$(this).removeClass "hover"
			
			# On ajoute la classe "deleted" au div .trash pour signifier que l'élément a bien été supprimé
			$(this).addClass "deleted"
			
			# On affiche un petit message "Cet élément a été supprimé" en récupérant la valeur textuelle de l'élément relaché
			#$(this).text ui.draggable.find(".item").text() + " removed !"
			
			# On supprimer l'élément de la page, le setTimeout est un fix pour IE (http://dev.jqueryui.com/ticket/4088)
			#setTimeout (->
			$('div[data-id='+ui.draggable.data("id")+']',".pageOfCyberfolioForProject").remove()
			ui.draggable.remove()
			ui.helper.remove()
			#), 1
			
			# On retourne à l'état originel de la poubelle après 2000 ms soit 2 secondes
			elt = $(this)
			setTimeout (->
				elt.removeClass "deleted"
				#elt.text "Trash"
			), 2000


	############################################################################################
	############################################################################################
	############################################################################################

	##############################################################
	# Masque toute les pages et charge les scripts associé
	#
	##############################################################
	allPages = new Array()

	structure = $('#structure')
	structure.hide()
	allPages.push structure
	#structureScript()


	page = $('#page')
	page.hide()
	allPages.push page
	#pageScript()

	style = $('#style')
	style.hide()
	allPages.push style
	#styleScript()

	projet = $('#projet')
	projet.hide()
	allPages.push projet
	#projetScript()

	reglage = $('#reglage')
	reglage.hide()
	allPages.push reglage
	#reglageScript()

	##############################################################
	# Start Backbone history a necessary step for bookmarkable URL's
	#
	##############################################################
	Backbone.history.start()