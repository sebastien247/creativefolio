<?php

namespace CreativeFolio\EditorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlocType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('poid')
            ->add('bloc')
            ->add('cyberfolio')
            //->add('layout')
            ->add('blocType')
            //->add('page')
        ;
    }

    public function getDefaultOptions(array $options)
    {
        $options = parent::getDefaultOptions($options);
        $options['csrf_protection'] = false;

        return $options;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CreativeFolio\EditorBundle\Entity\Bloc',
            'csrf_protection' => false,
        ));
    }

    public function getName()
    {
        return 'creativefolio_editorbundle_bloctype';
    }
}
