<?php

namespace CreativeFolio\EditorBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CyberfolioType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('url')
            //->add('layout')
            //->add('menu')
            //->add('style')
            //->add('user')
            ->add('categorie', null, array('required' => false, ));
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CreativeFolio\EditorBundle\Entity\Cyberfolio'
        ));
    }

    public function getName()
    {
        return 'creativefolio_editorbundle_cyberfoliotype';
    }
}
