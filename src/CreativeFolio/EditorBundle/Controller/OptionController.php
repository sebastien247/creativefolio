<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CreativeFolio\EditorBundle\Entity\Option;
use CreativeFolio\EditorBundle\Form\OptionType;

/**
 * Option controller.
 *
 */
class OptionController extends Controller
{
    /**
     * Lists all Option entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EditorBundle:Option')->findAll();

        return $this->render('EditorBundle:Option:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Option entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Option')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Option entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Option:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Option entity.
     *
     */
    public function newAction()
    {
        $entity = new Option();
        $form   = $this->createForm(new OptionType(), $entity);

        return $this->render('EditorBundle:Option:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Option entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Option();
        $form = $this->createForm(new OptionType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('option_show', array('id' => $entity->getId())));
        }

        return $this->render('EditorBundle:Option:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Option entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Option')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Option entity.');
        }

        $editForm = $this->createForm(new OptionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Option:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Option entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Option')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Option entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new OptionType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('option_edit', array('id' => $id)));
        }

        return $this->render('EditorBundle:Option:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Option entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EditorBundle:Option')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Option entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('option'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
