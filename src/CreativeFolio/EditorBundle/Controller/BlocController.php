<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CreativeFolio\EditorBundle\Entity\Bloc;
use CreativeFolio\EditorBundle\Form\BlocType;

/**
 * Bloc controller.
 *
 */
class BlocController extends Controller
{
    /**
     * Lists all Bloc entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EditorBundle:Bloc')->findAll();

        return $this->render('EditorBundle:Bloc:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Bloc entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Bloc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloc entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Bloc:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Bloc entity.
     *
     */
    public function newAction()
    {
        $entity = new Bloc();
        $form   = $this->createForm(new BlocType(), $entity);

        return $this->render('EditorBundle:Bloc:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Bloc entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Bloc();
        $form = $this->createForm(new BlocType(), $entity);

        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $cyberfolio = $em->getRepository('EditorBundle:Cyberfolio')->findOneByUser($user);

        if($request->getMethod() == 'POST')
        {

            $form->bind($request);

            if ($form->isValid())
            {
                $entity->setCyberfolio($cyberfolio);
                $em->persist($entity);
                $em->flush();

                //Instancier une "réponse" grâce à l'objet "Response"
                $response = new Response(json_encode( array ('id' => $entity->getId() ) ));
                //Lui indiquer le type de format dans le quelle est envoyé la reponse
                $response->headers->set('Content-Type', 'application/json');
                return $response;

                //return "id du bloc";
                //return $this->redirect($this->generateUrl('bloc_show', array('id' => $entity->getId())));
            }
        }

        return false;

        /*return $this->render('EditorBundle:Bloc:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));*/
    }

    /**
     * Displays a form to edit an existing Bloc entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Bloc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloc entity.');
        }

        $editForm = $this->createForm(new BlocType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Bloc:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Bloc entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Bloc')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Bloc entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new BlocType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bloc_edit', array('id' => $id)));
        }

        return $this->render('EditorBundle:Bloc:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Bloc entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EditorBundle:Bloc')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Bloc entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bloc'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
