<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CreativeFolio\EditorBundle\Entity\Cyberfolio;
use CreativeFolio\EditorBundle\Form\CyberfolioType;

use CreativeFolio\EditorBundle\Entity\Menu;
use CreativeFolio\EditorBundle\Entity\Style;
use CreativeFolio\EditorBundle\Entity\Layout;

/**
 * Cyberfolio controller.
 *
 */
class CyberfolioController extends Controller
{
	/**
	 * Lists all Cyberfolio entities.
	 *
	 */
	public function indexAction()
	{

		$em = $this->getDoctrine()->getManager();

		$entities = $em->getRepository('EditorBundle:Cyberfolio')->findAll();

		return $this->render('EditorBundle:Cyberfolio:index.html.twig', array(
			'entities' => $entities,
		));
	}

	/**
	* Méthode pour récupérer un user mais qui ne fonctionne pas...
	*/
	public function getUser()
	{
		$user = $this->container->get('security.context')->getToken()->getUser();
		if(!is_object($user) || !$user instanceof UserInterface)
			throw new AccessDeniedException('Vous n\'avez pas accès à cette section');

		return $user;
	}

	/**
	 * Finds and displays a Cyberfolio entity.
	 *
	 */
	public function showAction($id)
	{

		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Cyberfolio entity.');
		}

		$deleteForm = $this->createDeleteForm($id);

		return $this->render('EditorBundle:Cyberfolio:show.html.twig', array(
			'entity'      => $entity,
			'delete_form' => $deleteForm->createView(),        ));
	}

	/**
	 * Displays a form to create a new Cyberfolio entity.
	 *
	 */
	public function newAction()
	{
		$entity = new Cyberfolio();
		$form   = $this->createForm(new CyberfolioType(), $entity);

		return $this->render('EditorBundle:Cyberfolio:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	* Teste si le cyberfolio exite pour l'utilisateur
	*/
	public function ifExiste(){
		$user = $this->get('security.context')->getToken()->getUser();

		
        $em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->findByUser($user);
		
		// Si l'utilisateur possede deja un portfolio alors on le redirige vers l'editeur
		if ($entity) {
			return $this->redirect($this->generateUrl('editor'));
		}
	}

	/**
	 * Creates a new Cyberfolio entity.
	 *
	 */
	public function createAction(Request $request)
	{

		$user = $this->get('security.context')->getToken()->getUser();

		//$this->ifExiste();


		
        $em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->findByUser($user);
		
		// Si l'utilisateur possede deja un portfolio alors on le redirige vers l'editeur
		if ($entity) {
			return $this->redirect($this->generateUrl('editor'));
		}

		$entity  = new Cyberfolio();
		$form = $this->createForm(new CyberfolioType(), $entity);
		$form->bind($request);

		$user = $this->get('security.context')->getToken()->getUser();
		var_dump($user);  

		if ($form->isValid()) {

			$layout  = new Layout();
			$entity->setLayout($layout); // On lie le layout à notre cyberfolio
			$menu  = new Menu();
			$entity->setMenu($menu);
			$style = new Style();
			$entity->setStyle($style);

			$entity->setUser($user);

			$em = $this->getDoctrine()->getManager();
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('editor'));
		}

		return $this->render('EditorBundle:Cyberfolio:new.html.twig', array(
			'entity' => $entity,
			'form'   => $form->createView(),
		));
	}

	/**
	 * Displays a form to edit an existing Cyberfolio entity.
	 *
	 */
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Cyberfolio entity.');
		}

		$editForm = $this->createForm(new CyberfolioType(), $entity);
		$deleteForm = $this->createDeleteForm($id);

		return $this->render('EditorBundle:Cyberfolio:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * Edits an existing Cyberfolio entity.
	 *
	 */
	public function updateAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->find($id);

		if (!$entity) {
			throw $this->createNotFoundException('Unable to find Cyberfolio entity.');
		}

		$deleteForm = $this->createDeleteForm($id);
		$editForm = $this->createForm(new CyberfolioType(), $entity);
		$editForm->bind($request);

		if ($editForm->isValid()) {
			$em->persist($entity);
			$em->flush();

			return $this->redirect($this->generateUrl('cyberfolio_edit', array('id' => $id)));
		}

		return $this->render('EditorBundle:Cyberfolio:edit.html.twig', array(
			'entity'      => $entity,
			'edit_form'   => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		));
	}

	/**
	 * Deletes a Cyberfolio entity.
	 *
	 */
	public function deleteAction(Request $request, $id)
	{
		$form = $this->createDeleteForm($id);
		$form->bind($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$entity = $em->getRepository('EditorBundle:Cyberfolio')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Cyberfolio entity.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('cyberfolio'));
	}

	private function createDeleteForm($id)
	{
		return $this->createFormBuilder(array('id' => $id))
			->add('id', 'hidden')
			->getForm()
		;
	}
}
