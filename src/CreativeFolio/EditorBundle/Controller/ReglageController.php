<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ReglageController extends Controller
{
    public function indexAction()
    {
        return $this->render('EditorBundle:Reglage:index.html.twig');
    }
}
