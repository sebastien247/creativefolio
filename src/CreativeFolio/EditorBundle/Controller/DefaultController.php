<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
    	$user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();

		$entity = $em->getRepository('EditorBundle:Cyberfolio')->findByUser($user);

		// Si l'utilisateur possede deja un portfolio alors on le redirige vers l'editeur
		if (!$entity) {
			return $this->redirect($this->generateUrl('cyberfolio_new'));
		}

        return $this->render('EditorBundle:Default:index.html.twig');
    }

    public function lienCyberfolioAction(){
    	$em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $username = $user->getUsername();
        $host = $this->getRequest()->getHttpHost();
		$uri = $this->getRequest()->getRequestUri();
		$texte = preg_match('#^\/[a-zA-Z]*\/web/#',$uri, $matches);
		$path = "http://".$host.''.reset($matches)."cyberfolio/".$username;


        return $this->render('EditorBundle:Default:lienCyberfolio.html.twig', array(
            'path' => $path,
        ));
    }

    public function generateCyberfolioAction()
    {
		$em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();
        $username = $user->getUsername();
        var_dump($username);

        $entity = $em->getRepository('EditorBundle:Cyberfolio')->findOneByUser($user)->getStyle();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }

        $path = 'cyberfolio/'.$username;

        if (!is_dir($path)) {
		    mkdir($path, 0755, true);
		}

        $ostyles = json_decode($entity->getCss());

        $blocs = $ostyles->blocs;
        $pages = $ostyles->pages;
        $global = $ostyles->global;

		$sHTML_Header ='<!doctype html>
		<html lang="fr">
		<head>
		  <meta charset="utf-8">
		  <title>Titre de la page</title>
		  <link rel="stylesheet" href="style.css">
		  <script src="script.js"></script>
		</head>
		<body>';

		$sHTML_Footer = '</body>
		</html>';

		$sHTML_Content = '';
		$sCSS = '';

		var_dump($blocs);

		# Parcour les pages
		foreach ($pages as $currentPage) {

			if(isset($currentPage->blocsId)){

				$blocsOfPages = $currentPage->blocsId;
				# Parcour tout les blocs de la page
				foreach ($blocsOfPages as $idBloc) {
					# Compars l'id des blocs de la page avec le tableau de tout les blocs
					foreach ($blocs as $value) {
						if($value->id === $idBloc){
							//echo $currentPage->name." toto ".$idBloc;

							var_dump($value);

							if(isset($value->content)){
								$sHTML_Content .= "<div id=\"$value->id\">$value->content</div>";
							}else{
								$sHTML_Content .= "<div id=\"$value->id\"></div>";
							}

							//$sHTML_Content .= "<div id=\"$value->id\">$value->content</div>";
							$sCSS .= '[id="'.$value->id.'"]{';
							foreach ($value->style as $oKey => $oValue) {
								$sCSS .= $oKey.':'.$oValue.';';
							}
							$sCSS .= '}';

						}
					}

				}
			}
			$sHTML = $sHTML_Header."".$sHTML_Content."".$sHTML_Footer;

			$name = $currentPage->id."_".$currentPage->name;
			$filename = "$name.html";
			if($name === "0000000000001_home"){
				$filename = "index.html";
			}

			$fh = fopen($path."/".$filename, 'w') or die("can't open file");
			fwrite($fh, $sHTML);
			fclose($fh);

			var_dump($sHTML_Content);
			
			$sHTML_Content = '';


		}

		$sCSS .= 'body{';
		foreach ($global->style as $oKey => $oValue) {
			$sCSS .= $oKey.':'.$oValue.';';
		}
		$sCSS .= '}';

		# Bug Fix
		$sCSS .= 'ol,ul,li {list-style: none;}ul,li{margin: 0;padding: 0;border: 0;outline: 0;font-size: 100%;vertical-align: baseline;background: transparent;}';

		echo $sCSS;

		$filename = "style.css";
		$fh = fopen($path."/".$filename, 'w') or die("can't open file");
		fwrite($fh, $sCSS);
		fclose($fh);

		var_dump($sCSS);
		exit;
    }
}
