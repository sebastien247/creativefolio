<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class StructureController extends Controller
{
    public function indexAction()
    {
        return $this->render('EditorBundle:Structure:index.html.twig');
    }

    public function updateBlocOrderAction()
    {
    	echo json_encode($_GET['block_']);

    	foreach ($_GET['block_'] as $key => $value) {
	        $bloc = new Bloc();
			$bloc->setNom("Structure Bloc");
			$bloc->setPoid($value);
			echo "Création du bureau: ".$bloc->getId();
		}
		exit;
    }

    public function popupConfigGridAction(Request $request)
	{
		/*$user = $this->get('security.context')->getToken()->getUser();
		var_dump($user);


		$repository = $this->getDoctrine()->getRepository('UtilisateurBundle:Utilisateur');
		$userid = $repository->find($id);
        var_dump($users);*/


        $user = $this->container->get('security.context')->getToken()->getUser();
        
        return $this->container->get('templating')->renderResponse('EditorBundle:Structure:grid.html.'.$this->container->getParameter('fos_user.template.engine'), array('user' => $user));

	    // On initialise notre objet Desk
	   /*$option = new Option();
	    $option

	    $form = $this->createFormBuilder($option)
	        ->add('grid', 'text')
	        ->getForm(); // On récupère l'objet form 

	    if ($request->getMethod() == 'POST') { // Si on a soumis le formulaire
	        $form->bindRequest($request); // On bind les valeurs du POST à notre formulaire

	        if ($form->isValid()) { // On teste si les données entrées dans notre formulaire sont valides
	            
	            // On sauvegarde, redirige etc.

	        }
	    }*/
        //return $this->render('EditorBundle:Structure:grid.html.twig');
    }
}
