<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProjetController extends Controller
{
    public function indexAction()
    {
        return $this->render('EditorBundle:Projet:index.html.twig');
    }
}
