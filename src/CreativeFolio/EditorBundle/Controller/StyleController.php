<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CreativeFolio\EditorBundle\Entity\Style;
use CreativeFolio\EditorBundle\Form\StyleType;

/**
 * Style controller.
 *
 */
class StyleController extends Controller
{
    /**
     * Lists all Style entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EditorBundle:Style')->findAll();

        return $this->render('EditorBundle:Style:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Style entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Style')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Style:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Style entity.
     *
     */
    public function newAction()
    {
        $entity = new Style();
        $form   = $this->createForm(new StyleType(), $entity);

        return $this->render('EditorBundle:Style:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Style entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Style();
        $form = $this->createForm(new StyleType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('style_show', array('id' => $entity->getId())));

            /*//Instancier une "réponse" grâce à l'objet "Response"
            $response = new Response(json_encode( array ('style' => $entity ) ));
            //Lui indiquer le type de format dans le quelle est envoyé la reponse
            $response->headers->set('Content-Type', 'application/json');
            return $response;*/
        }

        //exit;//return false;

        return $this->render('EditorBundle:Style:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Style entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:Style')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }

        $editForm = $this->createForm(new StyleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:Style:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * JsonBlocsAction
    *
    */
    public function getBlocsJsonAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('EditorBundle:Cyberfolio')->findOneByUser($user)->getStyle();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }

        $response = new Response( json_encode( $entity->getCss() ) );

        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Edits an existing Style entity.
     *
     */
    public function updateOrderAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('EditorBundle:Cyberfolio')->findOneByUser($user)->getStyle();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }


        $editForm = $this->createForm(new StyleType(), $entity);

        // Blocs dans la Base De Données
        var_dump($entity->getCss());
        $css = json_encode( $entity->getCss() ,true);
        var_dump($css);

        var_dump("toto");

        // Recupere le nouveaux bloc dans le formulaire
        $editForm->bind($request);

        // Bloc a ajouter
        var_dump($entity->getCss());
        $css2 = json_decode( $entity->getCss() , true);

        var_dump($request->request);
        var_dump($css2);

        // Ajoute le bloc
        $css['blocs'] = $css2;

        // Encode en obj json
        $css = json_encode( $css );

        var_dump("Encoder : ");
        var_dump($css);

        if($request->getMethod() == 'POST')
        {
            //if ($editForm->isValid()) {
               /* $entity->setCss($css);
                $em->persist($entity);
                $em->flush();*/

                exit; //return $this->redirect($this->generateUrl('style_edit', array('id' => $id)));
            //}
        }

        var_dump("not");

        exit;
    }

    /**
     * Edits an existing Style entity.
     *
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->container->get('security.context')->getToken()->getUser();

        $entity = $em->getRepository('EditorBundle:Cyberfolio')->findOneByUser($user)->getStyle();

        //$entity = $em->getRepository('EditorBundle:Style')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Style entity.');
        }

       /* $deleteForm = $this->createDeleteForm($id);*/
        $editForm = $this->createForm(new StyleType(), $entity);

        // Blocs dans la Base De Données
        $css = json_decode( $entity->getCss() ,true);

        // Recupere le nouveaux bloc dans le formulaire
        $editForm->bind($request);

        // Bloc a ajouter
        //$css2 = json_decode( $entity->getCss() , true);
        $css = json_encode( $entity->getCss());

        // Ajoute le bloc
        /*$css['blocs'][] = $css2;

        // Tri par poids
        $tmp = Array();
        foreach($css['blocs'] as &$ma) 
            $tmp[] = &$ma["poid"];
        array_multisort($tmp, $css['blocs']);

        // Encode en obj json
        $css = json_encode( $css );

        var_dump("Encoder : ");
        var_dump($css);*/

        if($request->getMethod() == 'POST')
        {
            //if ($editForm->isValid()) {
                $entity->setCss($css);
                $em->persist($entity);
                $em->flush();

                exit; //return $this->redirect($this->generateUrl('style_edit', array('id' => $id)));
            //}
        }

        var_dump("not");

        exit;

        /*return $this->render('EditorBundle:Style:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));*/
    }

    /**
     * Deletes a Style entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EditorBundle:Style')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Style entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('style'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
