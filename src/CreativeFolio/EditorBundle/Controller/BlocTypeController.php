<?php

namespace CreativeFolio\EditorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CreativeFolio\EditorBundle\Entity\BlocType;
use CreativeFolio\EditorBundle\Form\BlocTypeType;

/**
 * BlocType controller.
 *
 */
class BlocTypeController extends Controller
{
    /**
     * Lists all BlocType entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('EditorBundle:BlocType')->findAll();

        return $this->render('EditorBundle:BlocType:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a BlocType entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:BlocType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlocType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:BlocType:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new BlocType entity.
     *
     */
    public function newAction()
    {
        $entity = new BlocType();
        $form   = $this->createForm(new BlocTypeType(), $entity);

        return $this->render('EditorBundle:BlocType:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new BlocType entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new BlocType();
        $form = $this->createForm(new BlocTypeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bloctype_show', array('id' => $entity->getId())));
        }

        return $this->render('EditorBundle:BlocType:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing BlocType entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:BlocType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlocType entity.');
        }

        $editForm = $this->createForm(new BlocTypeType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('EditorBundle:BlocType:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing BlocType entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('EditorBundle:BlocType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BlocType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new BlocTypeType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('bloctype_edit', array('id' => $id)));
        }

        return $this->render('EditorBundle:BlocType:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a BlocType entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('EditorBundle:BlocType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BlocType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('bloctype'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
