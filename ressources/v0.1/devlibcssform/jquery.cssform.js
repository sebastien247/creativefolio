//		CSS Form Builder
//		(c) 2013-2014 Sebastien Duboc


(function($) {

	$.fn.labelise = function(value) {
		if($.type(value) === "string") {
			$(this).prepend("<label>"+value+"</label>");
		}
	}

	$.fn.labeliseBefore = function(value) {
		if($.type(value) === "string") {
			$(this).before("<label>"+value+"</label>");
		}
	}

	$.fn.formMargin = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
			
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(d === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(d === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(d === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				};

				// Initialisation des div
				dname = "css-form-margin-"+d;
				ctn_margin = $("<div class='css-form css-form-margin " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-margin et css-form-margin-Left)
				ctn_slider_margin = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				ctn_text_margin = $("<div id='"+d+"'></div>");

				// ajoute la div "slider", dans la div "margin"
				ctn_margin.append(ctn_slider_margin);

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_margin.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_margin.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_margin.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_margin.labelise(obj.labelBottom)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				inputMargin = $('<input type="text" id="value" size="' + l + '"/>')
				$(inputMargin).addClass("margin"+d);
				ctn_margin.append(inputMargin);
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_margin);

				// Met a jour le slider
				$(inputMargin).on('keyup', this, function(event){
					var value = parseInt(this.value);
					$(event.currentTarget.previousSibling).slider({
						value: value
					});
				});

				// Bind le slider sur l'objet JQuery "margin"
				ctn_slider_margin.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("margin"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					},
					change: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("margin"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					}
				});


			}
		}
        // Permettre le chaînage par jQuery
		return this;
	};


	$.fn.formPadding = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(d === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(d === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(d === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				};

				// Initialisation des div
				dname = "css-form-padding-"+d;
				ctn_padding = $("<div class='css-form css-form-padding " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-padding et css-form-padding-Left)
				ctn_slider_padding = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				
				// ajoute la div "slider", dans la div "padding"
				ctn_padding.append(ctn_slider_padding);

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_padding.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_padding.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_padding.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_padding.labelise(obj.labelBottom)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length;				
				// Ajoute la balise input
				inputPadding = $('<input type="text" id="value" size="' + l + '"/>')
				$(inputPadding).addClass("padding"+d);
				ctn_padding.append(inputPadding);
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_padding);

				// Met a jour le slider
				$(inputPadding).on('keyup', this, function(event){
					var value = parseInt(this.value);
					$(event.currentTarget.previousSibling).slider({
						value: value
					});
				});

				// Bind le slider sur l'objet JQuery "padding"
				ctn_slider_padding.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("padding"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					},
					change: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("padding"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					}
				});
			}
		}
        // Permettre le chaînage par jQuery
		return this;
	};

	$.fn.formBorder = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",

			valueDefaultTopWidth: null,
			valueDefaultLeftWidth: null,
			valueDefaultBottomWidth: null,
			valueDefaultRightWidth: null,
			valueDefaultAllWidth: null,

			valueDefaultTopColor: null,
			valueDefaultLeftColor: null,
			valueDefaultBottomColor: null,
			valueDefaultRightColor: null,
			valueDefaultAllColor: null,

			valueDefaultTopStyle: null,
			valueDefaultLeftStyle: null,
			valueDefaultBottomStyle: null,
			valueDefaultRightStyle: null,
			valueDefaultAllStyle: null,

			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left,";
			}else if ( obj.directions === "global") {
				obj.directions = "";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};
			arrayCouleur = [];

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValueWidth = obj.valueDefaultLeftWidth;
					obj.defaultValueColor = obj.valueDefaultLeftColor;
					obj.defaultValueStyle = obj.valueDefaultLeftStyle;
				}else if(d === "Right"){
					obj.defaultValueWidth = obj.valueDefaultRightWidth;
					obj.defaultValueColor = obj.valueDefaultRightColor;
					obj.defaultValueStyle = obj.valueDefaultRightStyle;
				}else if(d === "Top"){
					obj.defaultValueWidth = obj.valueDefaultTopWidth;
					obj.defaultValueColor = obj.valueDefaultTopColor;
					obj.defaultValueStyle = obj.valueDefaultTopStyle;
				}else if(d === "Bottom"){
					obj.defaultValueWidth = obj.valueDefaultBottomWidth;
					obj.defaultValueColor = obj.valueDefaultBottomColor;
					obj.defaultValueStyle = obj.valueDefaultBottomStyle;
				}else if(d === ""){
					obj.defaultValueWidth = obj.valueDefaultAllWidth;
					obj.defaultValueColor = obj.valueDefaultAllColor;
					obj.defaultValueStyle = obj.valueDefaultAllStyle;
				};

				// Initialisation des div
				dname = "css-form-border-"+d;
				ctn_border = $("<div class='css-form css-form-border " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
				ctn_slider = $("<div class='css-form css-form-slider' id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				ctn_list = $("<div class='css-form css-form-list' id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
				ctn_color = $("<div class='css-form css-form-color' id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "color"
				// ajoute la div "slider", dans la div "border"
				ctn_border.append(ctn_slider);
				

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_border.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_border.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_border.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_border.labelise(obj.labelBottom)
				}else if(d === ""){
					ctn_border.labelise(obj.labelAll)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				inputBorder = $('<input type="text" id="value" size="' + l + '"/>')
				ctn_border.append(inputBorder);
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_border);

				// Met a jour le slider
				$(inputBorder).on('keyup', this, function(event){
					var value = parseInt(this.value);
					$(event.currentTarget.previousSibling).slider({
						value: value
					});
				});

				// Ajoute le tout dans l'element ciblé par l'utilisateur
				

				if(obj.defaultValueWidth.search("px") > 0){
					/*console.warn("la variable de bordure 'valueDefaultAllWidth' est inccorect");*/
					obj.defaultValueWidth = 0;
				}

				// Bind le slider sur l'objet JQuery "border"
				ctn_slider.slider({
					// Configuration du slider
					value: obj.defaultValueWidth,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValueWidth ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("border"+this.id+"Width", ui.value); 
						// Met a jour le css (borderLeft, ui.value)
					},
					change: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("border"+this.id+"Width", ui.value); // Met a jour le css (marginLeft, ui.value)
					}
				});
				
				// ajoute la div "list", dans la div "border"
				//ctn_border.append(ctn_list);

				
				
				//ajoute la liste des types de borders
				var selection = document.createElement('select');
				selection.id = d; // Direction
				selection.name = 'borderStyle';
				var element = document.createElement("option");
				element.value = 'hidden';
				element.selected = false;
				element.appendChild(document.createTextNode('hidden'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'solid';
				element.selected = false;
				element.appendChild(document.createTextNode('solid'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'dotted';
				element.selected = false;
				element.appendChild(document.createTextNode('dotted'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'dashed';
				element.selected = false;
				element.appendChild(document.createTextNode('dashed'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'double';
				element.selected = false;
				element.appendChild(document.createTextNode('double'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'groove';
				element.selected = false;
				element.appendChild(document.createTextNode('groove'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'ridge';
				element.selected = false;
				element.appendChild(document.createTextNode('ridge'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'inset';
				element.selected = false;
				element.appendChild(document.createTextNode('inset'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = 'outset';
				element.selected = false;
				element.appendChild(document.createTextNode('outset'));
				selection.appendChild(element);
				
				ctn_border.append(selection);

				// Autoselection des options
				$('option[value="'+obj.defaultValueStyle+'"]',selection).each(function(idx, elementx){
					console.log(elementx);
					$(elementx).prop('selected',true);
				});

				$(selection).change(function(event){
					$("option:selected", this).each(function() {
						$(obj.item).css("border"+event.currentTarget.id+"Style", $(this).text());
					})
				});

				var couleur = document.createElement("input");
				couleur.type = "text";
				couleur.id = "borderColor"+d;
				$(couleur).data("id", d);
				$(couleur).addClass("borderColor");
				couleur.size = "6";

				arrayCouleur.push(couleur);

				//ctn_color.append(couleur);
				ctn_border.append(couleur);
			}

			$(arrayCouleur).each(function(idx, elementx){
				$(elementx).ColorPicker({
					onChange: function(hsb, hex, rgb) {
						elementx.value = '#'+hex;
						$(obj.item).css("border"+$(elementx).data("id")+"Color", elementx.value);
					}
				});
			});

			$(".colorpicker").css("zIndex", "101");

		}
        // Permettre le chaînage par jQuery
		return this;
	};


	$.fn.formFont = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			fontsFamily: "toto",
			color: "#FF0000",
			fontSize: "1.2",
			labelFont: "Police : ",
			labelAlign: "Alignement : ",
			labelColor: "Couleur : ",
			labelSize: "Taille : ",
			textAlign: "center",
			min: 5,
    		max: 23,
    		valueDefault: 15,
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		//ctn_font = $("<div class='css-form css-form-font></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_list_family = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
		ctn_list_align = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
		ctn_color = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "color"
		ctn_slider = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
		ctn_font = $('<div/>', {}).addClass('css-form css-form-font');

		ctn_font.labelise(obj.labelFont);
	
		
		$target.append(ctn_font);
		
		//ajoute la liste des types de borders
		var selection = document.createElement('select');
		selection.name = 'fontStyle';

		arrayFontFamilly = obj.fontsFamily.split(";");

		for(i = 0; i < arrayFontFamilly.length; i++) {
			console.log(arrayFontFamilly[i]);
			var element = document.createElement("option");
			element.value = i;
			element.appendChild(document.createTextNode(arrayFontFamilly[i]));
			selection.appendChild(element);
		};


		ctn_font.append(selection);
		//ctn_list_family.append(selection);


		$(selection).change(function(event){
			$("option:selected", this).each(function() {
				$(obj.item).css("fontFamily", $(this).text());
				console.log($(this).text());
			})
		});


		var couleur_font = document.createElement("input");
		couleur_font.type = "text";
		couleur_font.id = "color";
		$(couleur_font).addClass("color");
		couleur_font.size = "6";
		//ctn_color.append(couleur);
		
		
		ctn_font.append(couleur_font);
		//$(couleur).labelise(obj.labelColor);

		$(couleur_font).each(function(idx, elementx){
			$(elementx).ColorPicker({
				onChange: function(hsb, hex, rgb) {
					elementx.value = '#'+hex;
					$(obj.item).css("color", elementx.value);
					console.log($(obj.item));
				}
			});
		});

		$(".colorpicker").css("zIndex", "101");

		//ajoute la liste des types de borders
		var selection = document.createElement('select');
		selection.name = 'textAlign';
		var element = document.createElement("option");
		element.value = '0';
		element.appendChild(document.createTextNode('center'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '1';
		element.appendChild(document.createTextNode('left'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '2';
		element.appendChild(document.createTextNode('right'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '3';
		element.appendChild(document.createTextNode('justify'));
		selection.appendChild(element);
		
		//ctn_font.append(ctn_list_align);
		
		ctn_font.append(selection);
		//$(selection).labelise(obj.labelAlign);
		//ctn_list_align.labeliseBefore(obj.labelAlign);

		$(selection).change(function(event){
			$("option:selected", this).each(function() {
				$(obj.item).css("textAlign", $(this).text());
				console.log($(this).text());
			})
		});

		//ctn_slider.labeliseBefore(obj.labelSize);
		ctn_font.append(ctn_slider);
		
		

		//ctn_slider.labeliseBefore(obj.labelSize);

		ctn_slider.slider({
			// Configuration du slider
			value: obj.defaultValue,
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: obj.min,
			max: obj.max,
			// Pendant quand l'utilisateur glisse le curseur
			create: function(event, ui) {
				$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
			},
			slide: function(event, ui) {
				$(obj.item).css("fontSize", ui.value); // Met a jour le css (paddingLeft, ui.value)
			}
		});
        // Permettre le chaînage par jQuery
		return this;
	};

		$.fn.formWidth = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			labelWidth: "Largeur : ",
			min: 0,
    		max: 400,
    		valueDefault: 15,
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		//ctn_width = $("<div class='css-form-width></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_slider_width = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
		ctn_width = $('<div/>', {}).addClass('css-form css-form-width');

		ctn_width.labelise(obj.labelWidth);
		$target.append(ctn_width);

		// Ajoute la balise input
		inputWidth = $('<input type="number" id="value"/>')
		$(inputWidth).addClass("width");
		ctn_width.append(inputWidth);
		
		// Met a jour le slider


		ctn_width.append(ctn_slider_width);
		ctn_slider_width.slider({
			// Configuration du slider
			value: obj.defaultValue,
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: obj.min,
			max: obj.max,
			// Pendant quand l'utilisateur glisse le curseur
			create: function(event, ui) {
				$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
			},
			slide: function(event, ui) {
				$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
				$(obj.item).css("width", ui.value); // Met a jour le css (marginLeft, ui.value)
			},
			change: function(event, ui) {
				$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
				$(obj.item).css("width", ui.value); // Met a jour le css (marginLeft, ui.value)
				console.log(obj.item);
			}

		});

		$(inputWidth).on('change', this, function(event){
			var value = parseInt(this.value);
			$(event.currentTarget.nextSibling).slider({
				value: value
			});
		});
		$(inputWidth).on('keyup', this, function(event){
			var value = parseInt(this.value);
			$(event.currentTarget.nextSibling).slider({
				value: value
			});
		});
        // Permettre le chaînage par jQuery
		return this;
	};


	$.fn.formHeight = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			labelHeight: "Hauteur : ",
			min: 0,
    		max: 400,
    		valueDefault: 15,
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		//ctn_height = $("<div class='css-form css-form-height></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_slider_height = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
		ctn_height = $('<div/>', {}).addClass('css-form css-form-height');

		ctn_height.labelise(obj.labelHeight);
		$target.append(ctn_height);

		// Ajoute la balise input
		inputHeight = $('<input type="number" id="value" min="0"/>')
		$(inputHeight).addClass("height");
		ctn_height.append(inputHeight);
		

		ctn_height.append(ctn_slider_height);
		ctn_slider_height.slider({
			// Configuration du slider
			value: obj.defaultValue,
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: obj.min,
			max: obj.max,
			// Pendant quand l'utilisateur glisse le curseur
			create: function(event, ui) {
				$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
			},
			slide: function(event, ui) {
				$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
				$(obj.item).css("height", ui.value); // Met a jour le css (marginLeft, ui.value)
			},
			change: function(event, ui) {
				$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
				$(obj.item).css("height", ui.value); // Met a jour le css (marginLeft, ui.value)
				console.log(obj.item);
			}
		});

		// Met a jour le slider
		$(inputHeight).on('change', this, function(event){
			var value = parseInt(this.value);
			$(event.currentTarget.nextSibling).slider({
				value: value
			});
		});
		$(inputHeight).on('keyup', this, function(event){
			var value = parseInt(this.value);
			$(event.currentTarget.nextSibling).slider({
				value: value
			});
		});

        // Permettre le chaînage par jQuery
		return this;
	};



$.fn.formPosition = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
			
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				D = d.substr(0,1).toUpperCase() + d.substr(1);


				if(D === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(D === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(D === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(D === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				};

				// Initialisation des div
				dname = "css-form-"+d;
				ctn_position = $("<div class='css-form css-form-"+ dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-margin et css-form-margin-Left)
				ctn_slider_position = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				ctn_list_position = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"

				// ajoute la div "slider", dans la div "margin"
				ctn_position.append(ctn_slider_position);

				// Ajoute les balises <label>
				if(D === "Left"){
					ctn_position.labelise(obj.labelLeft)
				}else if(D === "Right"){
					ctn_position.labelise(obj.labelRight)
				}else if(D === "Top"){
					ctn_position.labelise(obj.labelTop)
				}else if(D === "Bottom"){
					ctn_position.labelise(obj.labelBottom)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				inputPosition = $('<input type="number" id="value" size="' + l + '"/>')
				ctn_position.append(inputPosition);
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_position);

				// Met a jour le slider
				$(inputPosition).on('keyup', this, function(event){
					var value = parseInt(this.value);
					$(event.currentTarget.previousSibling).slider({
						value: value
					});
				});

				// Bind le slider sur l'objet JQuery "margin"
				ctn_slider_position.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						console.log(this);
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css(this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					},
					change: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css(this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					}


				});
			}

			//ajoute la liste des types de borders
			var selection = document.createElement('select');
			selection.name = 'textAlign';
			var element = document.createElement("option");
			element.value = '0';
			element.appendChild(document.createTextNode('absolute'));
			selection.appendChild(element);
			var element = document.createElement("option");
			element.value = '1';
			element.appendChild(document.createTextNode('relative'));
			selection.appendChild(element);
	
			ctn_position.append(ctn_list_position);
			ctn_list_position.append(selection);


			$(selection).change(function(event){
				$("option:selected", this).each(function() {
					$(obj.item).css("position", $(this).text());
					console.log($(this).text());
				})
			});



		}
        // Permettre le chaînage par jQuery
		return this;
	};





	$.fn.formBackgroundColor = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			color: "#FF0000",
			labelBackground: "Couleur de fond : ",
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		//ctn_background = $("<div class='css-form css-form-background></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_background_color = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "color"
		ctn_background = $('<div/>', {}).addClass('css-form css-form-background');

		// Ajoute le tout dans l'element ciblé par l'utilisateur
		$target.append(ctn_background);
		console.log($target);


		ctn_background.labelise(obj.labelBackground)

		var couleur = document.createElement("input");
		couleur.type = "text";
		couleur.id = "color";
		$(couleur).addClass("color");
		couleur.size = "6";
		ctn_background_color.append(couleur);
		ctn_background.append(couleur);

		$(couleur).each(function(idx, elementx){
			$(elementx).ColorPicker({
				onChange: function(hsb, hex, rgb) {
					elementx.value = '#'+hex;
					$(obj.item).css("backgroundColor", elementx.value);
				}
			});
		});

		$(".colorpicker").css("zIndex", "101");
        
        return this;
    };



    	$.fn.formOpacity = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			labelOpacity: "Opacite du bloc : ",
			min: 0,
    		max: 1,
    		valueDefault: 1,
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		//ctn_opacity = $("<div class='css-form css-form-opacity></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_slider_opacity = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
		ctn_opacity = $('<div/>', {}).addClass('css-form css-form-opacity');

		ctn_opacity.labelise(obj.labelOpacity);
		$target.append(ctn_opacity);

		// Ajoute la balise input
		inputOpacity = $('<input type="text" id="value" disabled/>')
		$(inputOpacity).addClass("opacity");
		ctn_opacity.append(inputOpacity);
		

		ctn_opacity.append(ctn_slider_opacity);
		ctn_slider_opacity.slider({
			// Configuration du slider
			value: obj.defaultValue,
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: obj.min,
			max: obj.max,
			// Pendant quand l'utilisateur glisse le curseur
			create: function(event, ui) {
				$("input#value", $(this).parent() ).val( obj.defaultValue/10 ); // Initialise le input ( data-value="4845118" )
			},
			slide: function(event, ui) {
				$("input#value", $(this).parent() ).val( ui.value/10 ); // Met a jour le input
				$(obj.item).css("opacity", ui.value/10); // Met a jour le css (marginLeft, ui.value)
			},
		});

        // Permettre le chaînage par jQuery
		return this;
	};

		$.fn.formContent = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			labelContent: "Ajouter du texte : ",
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		ctn_content = $("<div class='css-form css-form-content></div>");
		ctn_content = $('<div/>', {}).addClass('css-form css-form-content');

		ctn_content.labelise(obj.labelContent);
		$target.append(ctn_content);

		// Ajoute la balise input
		inputContent = $('<input type="text" id="value"/>');
		$(inputContent).addClass("content");
		ctn_content.append(inputContent);
		
		$(inputContent).on('keyup', this, function(event){
			divContent.html(this.value);
		});
		divContent = $("<div/>",{});
		$(obj.item).append(divContent);


		// Permettre le chaînage par jQuery
		return this;
	};
			$.fn.formFloat = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			labelFloat: "Float : ",
		};

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		ctn_float = $("<div class='css-form css-form-float></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_list_float = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
		ctn_float = $('<div/>', {}).addClass('css-form-float');

		ctn_float.labelise(obj.labelFloat);
		$target.append(ctn_float);
		

			//ajoute la liste des types de borders
			var selection = document.createElement('select');
			selection.name = 'float';
			var element = document.createElement("option");
			element.value = '0';
			element.appendChild(document.createTextNode('left'));
			selection.appendChild(element);
			var element = document.createElement("option");
			element.value = '1';
			element.appendChild(document.createTextNode('right'));
			selection.appendChild(element);
	
			ctn_float.append(ctn_list_float);
			ctn_list_float.append(selection);


			$(selection).change(function(event){
				$("option:selected", this).each(function() {
					$(obj.item).css("float", $(this).text());
					console.log($(this).text());
				})
			});

        // Permettre le chaînage par jQuery
		return this;
	};
})(jQuery);
