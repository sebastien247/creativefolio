$(function() {
return $("#style").formMargin({
  label: "Marge : ",
  min: 0,
  max: 400,
  directions: "all",
  item: "#bloc",
  labelLeft: "Marge gauche : ",
  labelTop: "Marge haut : ",
  labelBottom: "Marge bas : ",
  labelRight: "Marge droite : ",
  valueDefaultTop: "10",
  valueDefaultLeft: "10",
  valueDefaultBottom: "10",
  valueDefaultRight: "10"
}).formPadding({
  label: "Marge : ",
  min: 0,
  max: 400,
  directions: "all",
  item: "#bloc",
  labelLeft: "Remplissage gauche : ",
  labelTop: "Remplissage haut : ",
  labelBottom: "Remplissage bas : ",
  labelRight: "Remplissage droite : ",
  valueDefaultTop: "10",
  valueDefaultLeft: "10",
  valueDefaultBottom: "10",
  valueDefaultRight: "10"
}).formBorder({
  style: "solid",
  min: 0,
  max: 50,
  directions: "all",
  item: "#bloc",
  labelLeft: "Bordure gauche : ",
  labelTop: "Bordure haut : ",
  labelBottom: "Bordure bas : ",
  labelRight: "Bordure droite : ",
  labelAll: "Toutes les bordures : ",
  valueDefaultTopWidth: "10",
  valueDefaultLeftWidth: "10",
  valueDefaultBottomWidth: "10",
  valueDefaultRightWidth: "10",
  valueDefaultAllWidth: "10",
  valueDefaultTopColor: "#FF0000",
  valueDefaultLeftColor: "#FF0000",
  valueDefaultBottomColor: "#FF0000",
  valueDefaultRightColor: "#FF0000",
  valueDefaultAllColor: "#FF0000",
  valueDefaultTopStyle: "solid",
  valueDefaultLeftStyle: "solid",
  valueDefaultBottomStyle: "solid",
  valueDefaultRightStyle: "solid",
  valueDefaultAllStyle: "solid"
}).formWidth({
  item: "#bloc",
  labelWidth: "Largeur : ",
  defaultValue: 100,
  min: 0,
  max: 400,
}).formHeight({
  item: "#bloc",
  labelHeight: "Hauteur : ",
  defaultValue: 100,
  min: 0,
  max: 400,
}).formPosition({
  label: "Position : ",
  min: 0,
  max: 400,
  directions: "all",
  item: "#bloc",
  labelLeft: "Position gauche : ",
  labelTop: "Position haut : ",
  labelBottom: "Position bas : ",
  labelRight: "Position droite : ",
  valueDefaultTop: "10",
  valueDefaultLeft: "10",
  valueDefaultBottom: "10",
  valueDefaultRight: "10"
}).formBackgroundColor({
  labelBackground: "Couleur de fond : ",
  color: "#FF0000",
  item: "#bloc",
}).formOpacity({
  labelOpacity: "Opacite du bloc : ",
  min: 0,
  max: 10,  
  defaultValue: 10,
  item: "#bloc",
}).formFont({
  fontsFamily: "Arial,Comics sans ms, sans serif;Verdena, Roboto",
  color: "#FF0000",
  item: "#bloc",
  labelFont: "Police : ",
  labelAlign: "Alignement : ",
  labelColor: "Couleur : ",
  labelSize: "Taille : ",
  textAlign: "left",
  min: 5,
  max: 23
}).formContent({
  labelContent: "Ajouter du texte : ",
  item: "#bloc",
}).formFloat({
  labelFloat: "Float : ",
  item: "#bloc",
}).dialog();
});