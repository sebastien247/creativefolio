// Generated by CoffeeScript 1.4.0

$(function() {
  /*return $("#style").margin({
    label: "Marge : ",
    min: 0,
    max: 400,
    directions: "all",
    valueDefault: 20,
    item: "#bloc",
    labelLeft: "Marge gauche : ",
    labelTop: "Marge haut : ",
    labelBottom: "Marge bas : ",
    labelRight: "Marge droite : ",
    valueDefaultTop: 10,
    valueDefaultLeft: 5,
    valueDefaultBottom: 250,
    valueDefaultRight: 80
  }).backgroundColor({
    color: "#FF0055"
  }).padding({
    label: "Marge : ",
    min: 0,
    max: 400,
    directions: "all",
    valueDefault: 20,
    item: "#bloc",
    labelLeft: "Remplissage gauche : ",
    labelTop: "Remplissage haut : ",
    labelBottom: "Remplissage bas : ",
    labelRight: "Remplissage droite : ",
    valueDefaultTop: 10,
    valueDefaultLeft: 5,
    valueDefaultBottom: 250,
    valueDefaultRight: 80
  })

  .border({
    style: "solid",
    min: 0,
    max: 50,
    directions: "all",
    valueDefault: 5,
    item: "#bloc",
    labelLeft: "Bordure gauche : ",
    labelTop: "Bordure haut : ",
    labelBottom: "Bordure bas : ",
    labelRight: "Bordure droite : ",
    labelAll: "Toutes les bordures : ",
    valueDefaultTop: 0,
    valueDefaultLeft: 0,
    valueDefaultBottom: 0,
    valueDefaultRight: 0
  })*/

  return $("#style").font({
    fontFamily: "Arial",
    color: "#FF0000",
    valueDefault: 5,
    item: "#bloc",
    labelFont: "Police : ",
    textAlign: "left",
    min: 5,
    max: 23,
    valueDefault: 15,
  }).dialog();


});
