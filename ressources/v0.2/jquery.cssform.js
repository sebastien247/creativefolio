//		CSS Form Builder
//		(c) 2013-2014 Sebastien Duboc


(function($) {

	$.fn.labelise = function(value) {
		if($.type(value) === "string") {
			$(this).prepend("<label>"+value+"</label>");
		}
	}


	$.fn.margin = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
			
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(d === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(d === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(d === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				};

				// Initialisation des div
				dname = "css-form-margin-"+d;
				ctn_margin = $("<div class='css-form-margin " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-margin et css-form-margin-Left)
				ctn_slider = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				
				// ajoute la div "slider", dans la div "margin"
				ctn_margin.append(ctn_slider);

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_margin.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_margin.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_margin.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_margin.labelise(obj.labelBottom)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				ctn_margin.append('<input type="text" id="value" size="' + l + '" disabled/>');
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_margin);

				// Bind le slider sur l'objet JQuery "margin"
				ctn_slider.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("margin"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
					}
				});
			}
		}
        // Permettre le chaînage par jQuery
		return this;
	};


	$.fn.padding = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
			
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(d === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(d === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(d === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				};

				// Initialisation des div
				dname = "css-form-padding-"+d;
				ctn_padding = $("<div class='css-form-padding " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-padding et css-form-padding-Left)
				ctn_slider = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				
				// ajoute la div "slider", dans la div "padding"
				ctn_padding.append(ctn_slider);

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_padding.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_padding.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_padding.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_padding.labelise(obj.labelBottom)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				ctn_padding.append('<input type="text" id="value" size="' + l + '" disabled/>');
				
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_padding);

				// Bind le slider sur l'objet JQuery "padding"
				ctn_slider.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("padding"+this.id, ui.value); // Met a jour le css (paddingLeft, ui.value)
					}
				});
			}
		}
        // Permettre le chaînage par jQuery
		return this;
	};

	$.fn.border = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			min: 0,
			max: 100,
			item: this,
			directions: "all",
			valueDefaultTop: null,
			valueDefaultLeft: null,
			valueDefaultBottom: null,
			valueDefaultRight: null,
			labelLeft: null,
			labelTop: null,
			labelBottom: null,
			labelRight: null,
			color: "#FF0000",
			
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);
        console.log($target); // Element final a modifier

        if($.type(obj.directions) === "string") {
            if ( obj.directions === "all") {
				obj.directions = "top,right,bottom,left,";
			}else if ( obj.directions === "global") {
				obj.directions = "";
			}

			// Tableaux des directions
			n = obj.directions.split(",");
			obj.directions = {};

			for(i = 0; i < n.length; i++) {
				
				// Recupere la valeur numero "i" du tableaux "n"
				d = $.trim(n[i]); 
				// Premiere lettre en MAJUSCULE
				d = d.substr(0,1).toUpperCase() + d.substr(1);


				if(d === "Left"){
					obj.defaultValue = obj.valueDefaultLeft
				}else if(d === "Right"){
					obj.defaultValue = obj.valueDefaultRight
				}else if(d === "Top"){
					obj.defaultValue = obj.valueDefaultTop
				}else if(d === "Bottom"){
					obj.defaultValue = obj.valueDefaultBottom
				}else if(d === ""){
					obj.defaultValue = obj.valueDefaultAll
				};

				// Initialisation des div
				dname = "css-form-border-"+d;
				ctn_border = $("<div class='css-form-border " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
				ctn_slider = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
				ctn_list = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
				ctn_color = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "color"
				// ajoute la div "slider", dans la div "border"
				ctn_border.append(ctn_slider);
				

				// Ajoute les balises <label>
				if(d === "Left"){
					ctn_border.labelise(obj.labelLeft)
				}else if(d === "Right"){
					ctn_border.labelise(obj.labelRight)
				}else if(d === "Top"){
					ctn_border.labelise(obj.labelTop)
				}else if(d === "Bottom"){
					ctn_border.labelise(obj.labelBottom)
				}else if(d === ""){
					ctn_border.labelise(obj.labelAll)
				};

				// Calcule la longeur de l'entier
				l = obj.max.toString().length; 
				// Ajoute la balise input
				ctn_border.append('<input type="text" id="value" size="' + l + '" disabled/>');
				// Ajoute le tout dans l'element ciblé par l'utilisateur
				$target.append(ctn_border);


				// Bind le slider sur l'objet JQuery "border"
				ctn_slider.slider({
					// Configuration du slider
					value: obj.defaultValue,
					orientation: "horizontal",
					range: "min",
					animate: true,
					min: obj.min,
					max: obj.max,
					// Pendant quand l'utilisateur glisse le curseur
					create: function(event, ui) {
						$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
					},
					slide: function(event, ui) {
						$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
						$(obj.item).css("border"+this.id+"Width", ui.value); 
						// Met a jour le css (borderLeft, ui.value)
					}
				});
				
				// ajoute la div "list", dans la div "border"
				ctn_border.append(ctn_list);

				
				
				//ajoute la liste des types de borders
				var selection = document.createElement('select');
				selection.id = d; // Direction
				selection.name = 'borderStyle';
				var element = document.createElement("option");
				element.value = '0';
				element.appendChild(document.createTextNode('hidden'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '1';
				element.appendChild(document.createTextNode('solid'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '2';
				element.appendChild(document.createTextNode('dotted'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '3';
				element.appendChild(document.createTextNode('dashed'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '4';
				element.appendChild(document.createTextNode('double'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '5';
				element.appendChild(document.createTextNode('groove'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '6';
				element.appendChild(document.createTextNode('ridge'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '7';
				element.appendChild(document.createTextNode('inset'));
				selection.appendChild(element);
				var element = document.createElement("option");
				element.value = '8';
				element.appendChild(document.createTextNode('outset'));
				selection.appendChild(element);
				
				ctn_list.append(selection);


				$(selection).change(function(event){
					$("option:selected", this).each(function() {
						$(obj.item).css("border"+event.currentTarget.id+"Style", $(this).text());
					})
				});


				var couleur = document.createElement("input");
				couleur.type = "text";
				couleur.id = "borderColor"+d;
				$(couleur).data("id", d);
				$(couleur).addClass("borderColor");
				couleur.size = "6";
				ctn_color.append(couleur);
				ctn_border.append(ctn_color);
				
			}

			$(".borderColor").each(function(idx, elementx){
				$(elementx).ColorPicker({
					onChange: function(hsb, hex, rgb) {
						elementx.value = '#'+hex;
						$(obj.item).css("border"+$(elementx).data("id")+"Color", elementx.value);
					}
				});
			});

			$(".colorpicker").css("zIndex", "101");

		}
        // Permettre le chaînage par jQuery
		return this;
	};


	$.fn.font = function(options) {

		// Parametres par defaults
		var defaultOptions = {
			item: this,
			fontFamily: "Arial",
			color: "#FF0000",
			fontSize: "1.2",
			labelFont: "Police : ",
			textAlign: "center",
			min: 5,
    		max: 23,
    		valueDefault: 15,
		};

		var direction = undefined;

		// Fusionner les paramètres par défaut et ceux de l'utilisateur
        var obj = $.extend( defaultOptions, options);

        // Exprimer un nœud seul en objet jQuery
        var $target = $(this);// Element final a modifier

		// Initialisation des div

		ctn_font = $("<div class='css-form-font></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-border et css-form-border-Left)
		ctn_list = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "list"
		ctn_color = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "color"
		ctn_slider = $("<div></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
		ctn_font = $('<div/>', {}).addClass('css-form-font');

		ctn_font.labelise(obj.labelFont);
	
		ctn_font.append(ctn_list);
		$target.append(ctn_font);
		
		//ajoute la liste des types de borders
		var selection = document.createElement('select');
		selection.name = 'fontStyle';
		var element = document.createElement("option");
		element.value = '0';
		element.appendChild(document.createTextNode('Arial'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '1';
		element.appendChild(document.createTextNode('Times New Roman'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '2';
		element.appendChild(document.createTextNode('Verdana'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '3';
		element.appendChild(document.createTextNode('Roboto'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '4';
		element.appendChild(document.createTextNode('Sans Serif'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '4';
		element.appendChild(document.createTextNode('Comic sans MS'));
		selection.appendChild(element);
		
		ctn_list.append(selection);


		$(selection).change(function(event){
			$("option:selected", this).each(function() {
				$(obj.item).css("fontFamily", $(this).text());
				console.log($(this).text());
			})
		});


		var couleur = document.createElement("input");
		couleur.type = "text";
		couleur.id = "color";
		$(couleur).addClass("color");
		couleur.size = "6";
		ctn_color.append(couleur);
		ctn_font.append(ctn_color);

		$(".color").each(function(idx, elementx){
			$(elementx).ColorPicker({
				onChange: function(hsb, hex, rgb) {
					elementx.value = '#'+hex;
					$(obj.item).css("color", elementx.value);
				}
			});
		});

		$(".colorpicker").css("zIndex", "101");

		//ajoute la liste des types de borders
		var selection = document.createElement('select');
		selection.name = 'textAlign';
		var element = document.createElement("option");
		element.value = '0';
		element.appendChild(document.createTextNode('center'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '1';
		element.appendChild(document.createTextNode('left'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '2';
		element.appendChild(document.createTextNode('right'));
		selection.appendChild(element);
		var element = document.createElement("option");
		element.value = '3';
		element.appendChild(document.createTextNode('justify'));
		selection.appendChild(element);
		
		ctn_list.append(selection);


		$(selection).change(function(event){
			$("option:selected", this).each(function() {
				$(obj.item).css("textAlign", $(this).text());
				console.log($(this).text());
			})
		});

		ctn_font.append(ctn_slider);
		ctn_slider.slider({
			// Configuration du slider
			value: obj.defaultValue,
			orientation: "horizontal",
			range: "min",
			animate: true,
			min: obj.min,
			max: obj.max,
			// Pendant quand l'utilisateur glisse le curseur
			create: function(event, ui) {
				$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
			},
			slide: function(event, ui) {
				$(obj.item).css("fontSize", ui.value); // Met a jour le css (paddingLeft, ui.value)
			}
		});
        // Permettre le chaînage par jQuery
		return this;
		

	};



	$.fn.backgroundColor = function(options) {
		var defaultOptions = {
			color: null
		};
        var obj = $.extend( defaultOptions, options);
        var $target = $(this);

        

        return this;
    };
})(jQuery);
