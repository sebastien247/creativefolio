//		CSS Form Builder
//		(c) 2013-2014 Sebastien Duboc


(function($) {

		$.fn.labelise = function(value) {
			if($.type(value) === "string") {
				$(this).prepend("<label>"+value+"</label>");
			}
		}


		$.fn.margin = function(options) {

			// Parametres par defaults
			var defaultOptions = {
				min: 0,
				max: 100,
				item: this,
				directions: "all",
				valueDefaultTop: null,
				valueDefaultLeft: null,
				valueDefaultBottom: null,
				valueDefaultRight: null,
				labelLeft: null,
				labelTop: null,
				labelBottom: null,
				labelRight: null,
				
			};

			var direction = undefined;

			// Fusionner les paramètres par défaut et ceux de l'utilisateur
            var obj = $.extend( defaultOptions, options);

            // Exprimer un nœud seul en objet jQuery
            var $target = $(this); // Element final a modifier

            if($.type(obj.directions) === "string") {
	            if ( obj.directions === "all") {
					obj.directions = "top,right,bottom,left";
				}

				// Tableaux des directions
				n = obj.directions.split(",");
				obj.directions = {};

				for(i = 0; i < n.length; i++) {
					
					// Recupere la valeur numero "i" du tableaux "n"
					d = $.trim(n[i]); 
					// Premiere lettre en MAJUSCULE
					d = d.substr(0,1).toUpperCase() + d.substr(1);


					if(d === "Left"){
						obj.defaultValue = obj.valueDefaultLeft
					}else if(d === "Right"){
						obj.defaultValue = obj.valueDefaultRight
					}else if(d === "Top"){
						obj.defaultValue = obj.valueDefaultTop
					}else if(d === "Bottom"){
						obj.defaultValue = obj.valueDefaultBottom
					};

					// Initialisation des div
					dname = "css-form-margin-"+d;
					ctn_margin = $("<div class='css-form-margin " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-margin et css-form-margin-Left)
					ctn_slider = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
					
					// ajoute la div "slider", dans la div "margin"
					ctn_margin.append(ctn_slider);

					// Ajoute les balises <label>
					if(d === "Left"){
						ctn_margin.labelise(obj.labelLeft)
					}else if(d === "Right"){
						ctn_margin.labelise(obj.labelRight)
					}else if(d === "Top"){
						ctn_margin.labelise(obj.labelTop)
					}else if(d === "Bottom"){
						ctn_margin.labelise(obj.labelBottom)
					};

					// Calcule la longeur de l'entier
					l = obj.max.toString().length; 
					// Ajoute la balise input
					ctn_margin.append('<input type="text" id="value" size="' + l + '" disabled/>');
					
					// Ajoute le tout dans l'element ciblé par l'utilisateur
					$target.append(ctn_margin);

					// Bind le slider sur l'objet JQuery "margin"
					ctn_slider.slider({
						// Configuration du slider
						value: obj.defaultValue,
						orientation: "horizontal",
						range: "min",
						animate: true,
						min: obj.min,
						max: obj.max,
						// Pendant quand l'utilisateur glisse le curseur
						create: function(event, ui) {
							$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
						},
						slide: function(event, ui) {
							$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
							$(obj.item).css("margin"+this.id, ui.value); // Met a jour le css (marginLeft, ui.value)
						}
					});
				}
			}
            // Permettre le chaînage par jQuery
			return this;
		};


		$.fn.padding = function(options) {

			// Parametres par defaults
			var defaultOptions = {
				min: 0,
				max: 100,
				item: this,
				directions: "all",
				valueDefaultTop: null,
				valueDefaultLeft: null,
				valueDefaultBottom: null,
				valueDefaultRight: null,
				labelLeft: null,
				labelTop: null,
				labelBottom: null,
				labelRight: null,
				
			};

			var direction = undefined;

			// Fusionner les paramètres par défaut et ceux de l'utilisateur
            var obj = $.extend( defaultOptions, options);

            // Exprimer un nœud seul en objet jQuery
            var $target = $(this); // Element final a modifier

            if($.type(obj.directions) === "string") {
	            if ( obj.directions === "all") {
					obj.directions = "top,right,bottom,left";
				}

				// Tableaux des directions
				n = obj.directions.split(",");
				obj.directions = {};

				for(i = 0; i < n.length; i++) {
					
					// Recupere la valeur numero "i" du tableaux "n"
					d = $.trim(n[i]); 
					// Premiere lettre en MAJUSCULE
					d = d.substr(0,1).toUpperCase() + d.substr(1);


					if(d === "Left"){
						obj.defaultValue = obj.valueDefaultLeft
					}else if(d === "Right"){
						obj.defaultValue = obj.valueDefaultRight
					}else if(d === "Top"){
						obj.defaultValue = obj.valueDefaultTop
					}else if(d === "Bottom"){
						obj.defaultValue = obj.valueDefaultBottom
					};

					// Initialisation des div
					dname = "css-form-padding-"+d;
					ctn_padding = $("<div class='css-form-padding " + dname + "'></div>"); // 2 Classes definie : 1 general et 1 plus spécifique (css-form-padding et css-form-padding-Left)
					ctn_slider = $("<div id='"+d+"'></div>"); // Variable "d" enregistrer en id pour pouvoir la recuperer dans la function "slider"
					
					// ajoute la div "slider", dans la div "padding"
					ctn_padding.append(ctn_slider);

					// Ajoute les balises <label>
					if(d === "Left"){
						ctn_padding.labelise(obj.labelLeft)
					}else if(d === "Right"){
						ctn_padding.labelise(obj.labelRight)
					}else if(d === "Top"){
						ctn_padding.labelise(obj.labelTop)
					}else if(d === "Bottom"){
						ctn_padding.labelise(obj.labelBottom)
					};

					// Calcule la longeur de l'entier
					l = obj.max.toString().length; 
					// Ajoute la balise input
					ctn_padding.append('<input type="text" id="value" size="' + l + '" disabled/>');
					
					// Ajoute le tout dans l'element ciblé par l'utilisateur
					$target.append(ctn_padding);

					// Bind le slider sur l'objet JQuery "padding"
					ctn_slider.slider({
						// Configuration du slider
						value: obj.defaultValue,
						orientation: "horizontal",
						range: "min",
						animate: true,
						min: obj.min,
						max: obj.max,
						// Pendant quand l'utilisateur glisse le curseur
						create: function(event, ui) {
							$("input#value", $(this).parent() ).val( obj.defaultValue ); // Initialise le input ( data-value="4845118" )
						},
						slide: function(event, ui) {
							$("input#value", $(this).parent() ).val( ui.value ); // Met a jour le input
							$(obj.item).css("padding"+this.id, ui.value); // Met a jour le css (paddingLeft, ui.value)
						}
					});
				}
			}
            // Permettre le chaînage par jQuery
			return this;
		};

		$.fn.backgroundColor = function(options) {
			var defaultOptions = {
				color: null
			};
            var obj = $.extend( defaultOptions, options);
            var $target = $(this);

            

            return this;
        };



})(jQuery);